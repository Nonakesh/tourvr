﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ValueChanger : MonoBehaviour
{
	public string settingName;
	public Text valueText;

	public bool squareOutput;

	[Space]
	public bool hasMin;
	public int min;

	[Space]
	public bool hasMax;
	public int max;

	private int value;

	public void Start()
	{
		int pref = PlayerPrefs.GetInt(settingName);

		if (squareOutput)
		{
			value = Mathf.RoundToInt(Mathf.Sqrt(pref));
		}
		else
		{
			value = pref;
		}
		
		valueText.text = pref.ToString();
	}

	public void ChangeByValue(int valueChange)
	{
		value += valueChange;
		SaveSetting();
	}

	public void SetValue(int newValue)
	{
		if (squareOutput)
		{
			value = Mathf.RoundToInt(Mathf.Sqrt(newValue));
		}
		else
		{
			value = newValue;
		}
		
		SaveSetting();
	}

	public void Divide(int newValue)
	{
		value /= newValue;
		SaveSetting();
	}

	public void Multiply(int newValue)
	{
		value *= newValue;
		SaveSetting();
	}

	private void SaveSetting()
	{
		RestrictValue();

		int newPref;

		if (squareOutput)
		{
			newPref = value * value;
		}
		else
		{
			newPref = value;
		}

		valueText.text = newPref.ToString();

		PlayerPrefs.SetInt(settingName, newPref);
		PlayerPrefs.Save();
	}

	private void RestrictValue()
	{
		if (hasMin)
		{
			value = Mathf.Max(min, value);
		}

		if (hasMax)
		{
			value = Mathf.Min(max, value);
		}
	}
}
