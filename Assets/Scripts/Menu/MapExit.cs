﻿using UnityEngine;
using System.Collections;

public class MapExit : MonoBehaviour
{
	public TerrainGenerator generator;
	public IMapData mapLoader;

	public GameObject mapRoot;
	public GameObject mainMenu;

	public GameObject errorMessage;

	public Transform cameraTransform;

	public void ExitMap()
	{
		//Reset all map data and go back to the menu
		generator.Clear();
		mapLoader.ClearAll();
		mainMenu.SetActive(true);
		mapRoot.SetActive(false);

		errorMessage.SetActive(false);

		cameraTransform.position = Vector3.zero;
		cameraTransform.rotation = Quaternion.identity;
	}
}
