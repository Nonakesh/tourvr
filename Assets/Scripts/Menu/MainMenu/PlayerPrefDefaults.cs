﻿using System;
using UnityEngine;
using System.Collections;

public class PlayerPrefDefaults : MonoBehaviour
{
	public bool ResetSettings = true;

	public PlayerPrefIntValue[] intValues;
	public PlayerPrefStringValue[] stringValues;

	void Start ()
	{
		foreach (var value in intValues)
		{
			if (ResetSettings || !PlayerPrefs.HasKey(value.name))
			{
				PlayerPrefs.SetInt(value.name, value.value);
			}
		}

		foreach (var value in stringValues)
		{
			if (ResetSettings || !PlayerPrefs.HasKey(value.name))
			{
				PlayerPrefs.SetString(value.name, value.value);
			}
		}

		PlayerPrefs.Save();
	}

	[Serializable]
	public class PlayerPrefIntValue
	{
		public string name;
		public int value;
	}

	[Serializable]
	public class PlayerPrefStringValue
	{
		public string name;
		public string value;
	}
}
