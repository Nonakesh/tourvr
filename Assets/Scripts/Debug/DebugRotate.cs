﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugRotate : MonoBehaviour
{
#if UNITY_EDITOR
	public float speed = 0;

	public Camera[] cameras;

	private float yRot = 0;
	private float xRot = 0;

	private void Update()
	{
		if (!Input.GetMouseButton(1))
		{
			return;
		}

		yRot += Input.GetAxis("Mouse X") * speed;
		xRot -= Input.GetAxis("Mouse Y") * speed;

		foreach (var cam in cameras)
		{
			cam.transform.localRotation = Quaternion.Euler(xRot, yRot, 0);
		}
	}
#else
	private void Awake()
	{
		Destroy(this);
	}
#endif
}

