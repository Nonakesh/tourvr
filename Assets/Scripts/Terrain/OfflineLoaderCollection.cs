using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class OfflineLoaderCollection : ScriptableObject, IMapData
{
	public List<OfflineLoader> Loaders = new List<OfflineLoader>();

	private int qualityLevel = 0;

	public GeoBoundingBox CenterArea
	{
		get { return Loaders[qualityLevel].CenterArea; }
	}

	public List<LodArea> LODAreas
	{
		get { return Loaders[qualityLevel].LODAreas; }
	}

	public event Action<bool> MapLoaded;

	public void LoadMap()
	{
		LoadMap(PlayerPrefs.GetInt(SimpleSettings.TerrainSettings, 0));
	}

	public void LoadMap(int i)
	{
		qualityLevel = i;

		if (MapLoaded != null)
		{
			MapLoaded(false);
		}
	}

	public void LoadingFinished(bool isReloadingRoute)
	{
		isRunning = true;
	}

	[SerializeField]
	private GeoCoordinate[] coordinates;
	public GeoCoordinate[] Coordinates
	{
		get { return coordinates; }
		set { coordinates = value; }
	}

	[SerializeField]
	private Vector3[] path;
	public Vector3[] Path
	{
		get { return path; }
		set { path = value; }
	}

	private bool isRunning;
	public bool IsLoadingTextures { get { return !isRunning; } }
	public bool IsMapLoaded { get { return isRunning; } }

	public bool IsReloading
	{
		get { return false; }
	}

	public void LoadTerrain(bool b, bool isReloadingRoute, Vector3 transformPosition)
	{
		throw new NotImplementedException("Offline Route loading does not support flight paths");
	}

	public void ClearAll()
	{
		isRunning = false;
	}

#if UNITY_EDITOR
	/// <summary>
	//	This makes it easy to create, name and place unique new ScriptableObject asset files.
	/// </summary>
	public static void CreateAsset(OfflineLoaderCollection collection, string path)
	{
		string assetPathAndName = AssetDatabase.GenerateUniqueAssetPath(path + ".asset");

		PathContainer[][] containers = new PathContainer[collection.Loaders.Count][];

		//Save textures
		for (var i = 0; i < collection.Loaders.Count; i++)
		{
			var loader = collection.Loaders[i];
			containers[i] = new PathContainer[loader.LODAreas.Count];

			for (var j = 0; j < loader.LODAreas.Count; j++)
			{
				var area = loader.LODAreas[j];

				string texPath = path + string.Format("_colorMap{0}-{1}.png", i, j);
				File.WriteAllBytes(texPath, area.colorMap.EncodeToPNG());

				containers[i][j].ColorMap = texPath;
				containers[i][j].HeightMaps = new string[area.heightMaps.Length];

				for (int k = 0; k < area.heightMaps.Length; k++)
				{
					texPath = path + string.Format("_heightMap{0}-{1}-{2}.png", i, j, k);
					File.WriteAllBytes(texPath, area.heightMaps[k].EncodeToPNG());
					area.heightMaps[k] = (Texture2D) AssetDatabase.LoadAssetAtPath(texPath, typeof(Texture2D));

					containers[i][j].HeightMaps[k] = texPath;
				}
			}
		}

		//Refresh database
		AssetDatabase.Refresh();

		//Load all textures
		for (var i = 0; i < collection.Loaders.Count; i++)
		{
			var loader = collection.Loaders[i];
			for (var j = 0; j < loader.LODAreas.Count; j++)
			{
				var area = loader.LODAreas[j];

				area.colorMap = (Texture2D) AssetDatabase.LoadAssetAtPath(containers[i][j].ColorMap, typeof(Texture2D));

				for (int k = 0; k < area.heightMaps.Length; k++)
				{
					area.heightMaps[k] = (Texture2D)AssetDatabase.LoadAssetAtPath(containers[i][j].HeightMaps[k], typeof(Texture2D));
				}
			}
		}

		AssetDatabase.CreateAsset(collection, assetPathAndName);

		AssetDatabase.SaveAssets();
		AssetDatabase.Refresh();
		EditorUtility.FocusProjectWindow();
		Selection.activeObject = collection;
	}

	private struct PathContainer
	{
		public string ColorMap;
		public string[] HeightMaps;
	}
#endif
}

[Serializable]
public class OfflineLoader
{
	public GeoBoundingBox CenterArea;
	public List<LodArea> LODAreas;
}