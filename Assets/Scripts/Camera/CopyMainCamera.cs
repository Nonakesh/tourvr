﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Camera))]
public class CopyMainCamera : MonoBehaviour
{
	public Camera mainCamera;

	private Camera _ownCamera;

	private Camera OwnCamera
	{
		get { return _ownCamera ?? (_ownCamera = GetComponent<Camera>()); }
	}


	void LateUpdate ()
	{
		OwnCamera.CopyFrom(mainCamera);
		transform.position = mainCamera.transform.position;
		transform.rotation = mainCamera.transform.rotation;
		OwnCamera.depth -= 1;
	}
}
