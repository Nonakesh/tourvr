﻿using UnityEngine;
using System.Collections;
using UnityEngine.VR;

public class VREyeHeight : MonoBehaviour
{
	public float eyeHeight = 1.7f;

	void Start ()
	{
		//HTC Vive manages the head position on its own, for everything change the head-position 
		//(so the camera isn't inside the floor)
		if (VRDevice.model != "Vive MV")
		{
			transform.position += Vector3.up * eyeHeight;
		}
	}
}
