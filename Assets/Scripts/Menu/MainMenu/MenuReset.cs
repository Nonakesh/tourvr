﻿using UnityEngine;
using System.Collections;

public class MenuReset : MonoBehaviour
{
	public GameObject[] active;
	public GameObject[] inactive;

	void Awake ()
	{
		foreach (var obj in active)
		{
			obj.SetActive(true);
		}

		foreach (var obj in inactive)
		{
			obj.SetActive(false);
		}
	}
}
