﻿using System;
using System.Linq;
using UnityEngine;

//From http://gis.stackexchange.com/questions/75528/length-of-a-degree-where-do-the-terms-in-this-formula-come-from
public static class LongLatUtility 
{
	private const double m1 = 111132.92;     // latitude calculation term 1
    private const double m2 = -559.82;       // latitude calculation term 2
    private const double m3 = 1.175;         // latitude calculation term 3
    private const double m4 = -0.0023;       // latitude calculation term 4
    private const double p1 = 111412.84;     // longitude calculation term 1
    private const double p2 = -93.5;         // longitude calculation term 2
    private const double p3 = 0.118;         // longitude calculation term 3

	private const double degToRad = Math.PI / 180.0;
	private const double radToDeg = 180.0 / Math.PI;

	//This is approximated, as the length changes with latitude, but should still be close enough for our purpose
	public static double MeterToLatitudeDegrees(double meter, GeoBoundingBox boundingBox)
	{
		return meter / Math.Max(CalcLatitudeDegreeMeters(boundingBox.north), CalcLatitudeDegreeMeters(boundingBox.south));
	}

	public static double MeterToLatitudeDegrees(double meter, double latitude)
	{
		return meter / CalcLatitudeDegreeMeters(latitude);
	}

	public static double MeterToLongitudeDegrees(double meter, GeoBoundingBox boundingBox)
	{
		return meter / Math.Max(CalcLongitudeDegreeMeters(boundingBox.north), CalcLongitudeDegreeMeters(boundingBox.south));
	}

	public static double MeterToLongitudeDegrees(double meter, double latitude)
	{
		return meter / CalcLongitudeDegreeMeters(latitude);
	}

	public static double CalcLatitudeDegreeMeters(double lat)
	{
		lat *= degToRad;
		return m1 + (m2 * Math.Cos(2 * lat)) + (m3 * Math.Cos(4 * lat)) + (m4 * Math.Cos(6 * lat));
	}

	public static double CalcLongitudeDegreeMeters(double lat)
	{
		lat *= degToRad;
		return (p1 * Math.Cos(lat)) + (p2 * Math.Cos(3 * lat)) + (p3 * Math.Cos(5 * lat));
	}

	//from https://msdn.microsoft.com/en-us/library/bb259689.aspx
	public static GeoTileCoordinate CalcTilePosition(double longitude, double latitude, int level)
	{
		GeoTileCoordinate target;

		int tileCount = GetTileCount(level);
		double sinLatitude = Math.Sin(latitude * Math.PI / 180.0);
		target.x = ((longitude + 180) / 360) * tileCount;
		target.y = (0.5 - Math.Log((1 + sinLatitude) / (1 - sinLatitude)) / (4 * Math.PI)) * tileCount;

		return target;
	}

	public static int GetTileCount(int level)
	{
		return Convert.ToInt32(Math.Pow(2, level));
	}

	public static bool IsInBoundingBox(GeoBoundingBox bBox, double longitude, double latitude)
	{
		return (bBox.south < latitude && bBox.north > latitude) &&
			   ((bBox.west < longitude && bBox.east > longitude) || ((bBox.west < longitude || bBox.east > longitude) && bBox.west > bBox.east));
	}

	public static bool IsInBoundingBox(GeoBoundingBox bBox, GeoCoordinate coord)
	{
		return IsInBoundingBox(bBox, coord.longitude, coord.latitude);
	}

	public static bool IsInBoundingBox(GeoBoundingBox outer, GeoBoundingBox inner)
	{
		return IsInBoundingBox(outer, inner.Start) && IsInBoundingBox(outer, inner.End);
	}

	//This function only works for routes that are smaller than 180° longitude and are not on the poles
	public static GeoBoundingBox CalcBoundingBox(GeoCoordinate[] coords)
	{
		GeoBoundingBox box = new GeoBoundingBox(coords[0].latitude, coords[0].longitude, coords[0].latitude, coords[0].longitude);

		foreach (var coord in coords.Skip(1))
		{
			if (coord.latitude < box.south)
			{
				box.south = coord.latitude;
			}

			if (coord.latitude > box.north)
			{
				box.north = coord.latitude;
			}

			double lNew = coord.longitude;
			double lOld = box.west;

			//If the coordinates jump over the -180 to 180 degree border, add 360 to the negative one
			if (Math.Abs(lNew - lOld) > 180)
			{
				lNew = AngleToPositive(lNew);
				lOld = AngleToPositive(lOld);
			}
			if (lNew < lOld)
			{
				box.west = ToRealAngle(lNew);
			}

			lNew = coord.longitude;
			lOld = box.east;

			//If the coordinates jump over the -180 to 180 degree border, add 360 to the negative one
			if (Math.Abs(lNew - lOld) > 180)
			{
				lNew = AngleToPositive(lNew);
				lOld = AngleToPositive(lOld);
			}
			if (lNew > lOld)
			{
				box.east = ToRealAngle(lNew);
			}
		}

		return box;
	}

	public static GeoBoundingBox ExpandBoundingBox(GeoBoundingBox box, double border)
	{
		box.north += MeterToLatitudeDegrees(border, box.north);
		box.south -= MeterToLatitudeDegrees(border, box.south);

		//Find larger meter size (north or south), so it has at least the specified border
		var southLongDist = MeterToLongitudeDegrees(border, box.south);
		var northLongDist = MeterToLongitudeDegrees(border, box.north);
		var longDist = Math.Max(southLongDist, northLongDist);

		box.west = ToRealAngle(box.west - longDist);
		box.east = ToRealAngle(box.east + longDist);

		return box;
	}

	public static GeoBoundingBox SetMinBoundingBox(GeoBoundingBox box, double minSize)
	{
		double latMeter = CalcMaxLatitudeDegreeMeter(box);
		double longMeter = CalcMaxLongitudeDegreeMeter(box);

		Vector2 meterSize = CalcMeterSize(box);

		if (meterSize.x < minSize)
		{
			double diff = minSize - meterSize.x;
			box.west = ToRealAngle(box.west - diff * 0.5 / longMeter);
			box.east = ToRealAngle(box.east + diff * 0.5 / longMeter);
		}

		if (meterSize.y < minSize)
		{
			double diff = minSize - meterSize.y;
			box.south = box.south - diff * 0.5 / latMeter;
			box.north = box.north + diff * 0.5 / latMeter;
		}

		return box;
	}

	public static double AngleToPositive(double angle)
	{
		angle = Mathd.Mod(angle, 360);

		if (angle > 0)
		{
			return angle;
		}

		return angle + 360;
	}

	public static double ToRealAngle(double angle)
	{
		angle = Mathd.Mod(angle, 360);

		if (angle > 180)
		{
			return angle - 360;
		}

		return angle;
	}

	public static Vector2 CalcMeterSize(GeoBoundingBox border)
	{
		var longitudeMeter = Math.Max(CalcLongitudeDegreeMeters(border.north),
			CalcLongitudeDegreeMeters(border.south));

		var latitudeMeter = Math.Max(CalcLatitudeDegreeMeters(border.north),
			CalcLatitudeDegreeMeters(border.south));

		var widthDegree = Mathd.ModularDistance(border.west, border.east, 360.0);
		var heightDegree = border.north - border.south;

		return new Vector2((float) (widthDegree * longitudeMeter), (float) (heightDegree * latitudeMeter));
	}

	public static double CalcMaxLongitudeDegreeMeter(GeoBoundingBox border)
	{
		return Math.Max(CalcLongitudeDegreeMeters(border.north),
			CalcLongitudeDegreeMeters(border.south));
	}

	public static double CalcMaxLatitudeDegreeMeter(GeoBoundingBox border)
	{
		return Math.Max(CalcLatitudeDegreeMeters(border.north),
			CalcLatitudeDegreeMeters(border.south));
	}

	public static GeoBoundingBox MakeSquare(GeoBoundingBox centerArea)
	{
		var longitudeMeter = Math.Max(CalcLongitudeDegreeMeters(centerArea.north),
			CalcLongitudeDegreeMeters(centerArea.south));

		var latitudeMeter = Math.Max(CalcLatitudeDegreeMeters(centerArea.north),
			CalcLatitudeDegreeMeters(centerArea.south));

		var widthDegree = Mathd.ModularDistance(centerArea.west, centerArea.east, 360.0);
		var lengthDegree = centerArea.north - centerArea.south;

		double meterWidth = longitudeMeter * widthDegree;
		double meterLength = latitudeMeter * lengthDegree;

		if (meterWidth > meterLength)
		{
			double aspect = meterWidth / meterLength;
			meterLength *= aspect;
		}
		else
		{
			double aspect = meterLength / meterWidth;
			meterWidth *= aspect;
		}

		meterWidth *= 0.5;
		meterLength *= 0.5;

		widthDegree = meterWidth / longitudeMeter;
		lengthDegree = meterLength / latitudeMeter;

		var center = centerArea.Center;

		return new GeoBoundingBox
		{
			south = ToRealAngle(center.latitude - lengthDegree),
			north = ToRealAngle(center.latitude + lengthDegree),
			west = ToRealAngle(center.longitude - widthDegree),
			east = ToRealAngle(center.longitude + widthDegree),
		};
	}
}
