using System;
using System.Collections.Generic;
using UnityEngine;

public static class TerrainUtility
{
	public static int CalcLODCount(GeoBoundingBox center)
	{
		double terrainSize = PlayerPrefs.GetInt("MinViewDistanceKM") * 1000;

		return (int)Math.Ceiling(Math.Log(terrainSize / LongLatUtility.CalcMeterSize(center).x, 3));
	}

	public static float GetHeightValue(List<LodArea> areas, double longitude, double latitude)
	{
		//Find the most detailed height map where the requested position is contained.
		LodArea lodArea = null;
		foreach (var area in areas)
		{
			if (LongLatUtility.IsInBoundingBox(area.bounds, longitude, latitude))
			{
				lodArea = area;
				break;
			}
		}

		if (lodArea == null)
		{
			//Debug.LogError("Area out of bounds, can't get height. " + longitude + " " + latitude);
			return 0;
		}

		//Get the encoded value from the height map
		var coord = LongLatUtility.CalcTilePosition(longitude, latitude, lodArea.zoomLevel);

		float u = (float)coord.x;
		float v = (float)coord.y;

		int x = (int)u - lodArea.heightmapStartX;
		int y = (int)v - lodArea.heightmapStartY;

		u -= Mathf.FloorToInt(u);
		v -= Mathf.FloorToInt(v);

		return DecodeHeight(lodArea.heightMaps[x, y].GetPixelBilinear(u, 1 - v));
	}

	private static float DecodeHeight(Color c)
	{
		c *= 255;
		return c.r * 256.0f + c.g + c.b / 256.0f - 32768;
	}
}