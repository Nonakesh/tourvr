﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;

[AddComponentMenu("StixGames/Displacement Trail Renderer")]
[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
public class PathRenderer : MonoBehaviour
{
	public MapLoader mapLoader;

    public float minVertexDistance = 0.5f;

	public float width = 2;

    public Material material;

    private Vector3 upDir = Vector3.up;

    private Mesh mesh;

    private List<Vector3> vertices = new List<Vector3>();
    private List<int> triangles = new List<int>();
    private List<Vector2> uvs = new List<Vector2>();
    private List<Vector3> normals = new List<Vector3>();
    private List<Vector4> tangents = new List<Vector4>();

	public void Awake()
	{
		mapLoader.CoordinatesLoaded += coords =>
			GeneratePath(mapLoader.Path);
	}

    public void GeneratePath(Vector3[] points)
    {
        mesh = new Mesh();

		if (points.Length < 2)
		{
			return;
		}

		float uvFactor = 1.0f / (points.Length - 1);

		//Iterate though all previous points
		for (int i = 0; i < points.Length; i++)
		{
			//First point
			Vector3 point = points[i];
			if (i == 0)
			{
				AddPoint(point, points[i + 1] - point, 0);
				continue;
			}

			//Last point
			Vector3 lastPoint = points[i - 1];
			if (i == points.Length - 1)
			{
				AddPoint(point, point - lastPoint, 1);
				break;
			}

			//In-between points
			Vector3 nextPoint = points[i + 1];

			AddPoint(point, nextPoint - lastPoint, i * uvFactor);
		}

		mesh.SetVertices(vertices);
		mesh.SetTriangles(triangles, 0);
		mesh.SetUVs(0, uvs);
		mesh.SetNormals(normals);
		mesh.SetTangents(tangents);

	    GetComponent<MeshFilter>().sharedMesh = mesh;
	    GetComponent<MeshRenderer>().sharedMaterial = material;
    }

	private void AddPoint(Vector3 point, Vector3 direction, float uv)
    {
        float halfWidth = width;

        direction.Normalize();
        Vector3 right = Vector3.Cross(upDir, direction);

        vertices.Add(point - right * halfWidth);
        vertices.Add(point + right * halfWidth);
        uvs.Add(new Vector2(0, uv));
        uvs.Add(new Vector2(1, uv));
        normals.Add(upDir);
        normals.Add(upDir);
        tangents.Add(new Vector4(direction.x, direction.y, direction.z, 1));
        tangents.Add(new Vector4(direction.x, direction.y, direction.z, 1));

        int lastVert = vertices.Count-1;
        if (lastVert >= 3)
        {
            triangles.Add(lastVert - 1);
            triangles.Add(lastVert);
            triangles.Add(lastVert - 2);

            triangles.Add(lastVert - 2);
            triangles.Add(lastVert - 3);
            triangles.Add(lastVert - 1);
        }
    }
}