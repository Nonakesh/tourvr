﻿//#define COLLECT_STATS

using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine.UI;

[RequireComponent(typeof(TerrainGenerator))]
public class MapLoader : MonoBehaviour, IMapData
{
	public GameObject errorMessage;
	public Text errorMessageText;
	[TextArea]
	public string errorMessageString;

	[Header("Online Mode")]
	public string coordinateLink;

	[Header("Offline Mode")]
	public bool offlineMode = false;
	public string offlineRouteName;
	[HideInInspector]
	public string offlineData;

	[Header("Loader Settings")]
	public int maxHeightmapZoom = 14;
	public double centerBorder = 50;
	public double minCenterSize = 5000;
	public double flightModeBorder = 500;
	public double textureLoadingBoundary = 1.05;
	public LodLevelSettings[] lodLevels;

	public GeoCoordinate[] Coordinates { get; private set; }

	public GeoBoundingBox CenterArea { get; private set; }

	public Vector3[] Path { get; private set; }

	public bool IsLoadingTextures { get; private set; }
	public bool IsMapLoaded { get; private set; }
	public bool IsReloading { get; private set; }

	/// <summary>
	/// The level of detail areas, detail decreasing with increasing index
	/// </summary>
	public List<LodArea> LODAreas { get; private set; }

	private string bingApiKey;
	private string mapzenKey;

#if UNITY_EDITOR && COLLECT_STATS
	private string dataCollection = @"C:\Users\simon\Documents\Unity Projects\VRMaps\DataCollection\Rad\";
	private int colorMapBytes = 0;
	private int heightMapBytes = 0;
#endif

	public event Action<GeoCoordinate[]> CoordinatesLoaded;
	public event Action<bool> MapLoaded;

	public void LoadTerrain(bool useFlightMode, bool isReloadingRoute)
	{
		StartCoroutine(LoadTerrainData(useFlightMode, isReloadingRoute, false));
	}

	public void LoadTerrain(bool useFlightMode, bool isReloadingRoute, Vector3 flightModePosition)
	{
		StartCoroutine(LoadTerrainData(useFlightMode, isReloadingRoute, true, flightModePosition));
	}

	private void OnEnable()
	{
		Initialize();
	}

	private TerrainGenerator _generator;
	private TerrainGenerator Generator {get { return _generator ?? (_generator = GetComponent<TerrainGenerator>()); } }

	private bool isInitialized;
	private void Initialize()
	{
		if (isInitialized)
		{
			return;
		}

		isInitialized = true;

		bingApiKey = Resources.Load<TextAsset>("bingMaps").text;
		mapzenKey = Resources.Load<TextAsset>("mapzen").text;
		LODAreas = new List<LodArea>();

		//After the terrain was generated, delete all textures to safe memory
		Generator.TerrainGenerated += OnTerrainGenerated;
	}

	//private void OnDisable()
	//{
	//	GetComponent<TerrainGenerator>().TerrainGenerated -= OnTerrainGenerated;
	//}

	private void OnTerrainGenerated(bool isReloadingRoute)
	{
		Clear();

		IsMapLoaded = true;
	}

	private IEnumerator LoadTerrainData(bool useFlightMode, bool isReloadingRoute, bool useFlightPosition, 
		Vector3 flightPosition = new Vector3())
	{
#if UNITY_EDITOR
		var startTime = Time.realtimeSinceStartup;
#endif

		errorMessage.SetActive(false);

		//If there is a route being loaded right now, don't start loading another one.
		if (IsLoadingTextures || Generator.IsGeneratingTerrain)
		{
			yield break;
		}

		try
		{
			if (!isReloadingRoute)
			{
				IsMapLoaded = false;
			}

			IsLoadingTextures = true;
			IsReloading = isReloadingRoute;

			Initialize();

			//Create LOD Levels
			int lodLevelCount = PlayerPrefs.GetInt("LODLevels", 3);
			int lodLevelResolution = PlayerPrefs.GetInt("TextureSize", 512);
			int lodLevelHeightmaps = PlayerPrefs.GetInt("Heightmaps", 1);
			int textureSplitLOD = PlayerPrefs.GetInt("SplitTextureLODs", 0);

			lodLevels = new LodLevelSettings[lodLevelCount];
			for (int i = 0; i < lodLevelCount; i++)
			{
				lodLevels[i] = new LodLevelSettings
				{
					colorMapResolution = lodLevelResolution,
					maxHeightmapTiles = lodLevelHeightmaps
				};
			}

			//Clear data
			Clear();

			//Load coordinates, don't reload them if they've already been loaded once
			if (Coordinates == null || Coordinates.Length == 0)
			{
				yield return LoadCoordinates();
			}

			//Calculate bounding box
			GeoCoordinate[] terrainCenter;
			double border;
			if (useFlightMode)
			{
				if (useFlightPosition)
				{
					var flightCoord = GeoCoordinate.FromPosition(flightPosition, CenterArea);
					terrainCenter = new[] {flightCoord};
				}
				else
				{
					terrainCenter = new[] {Coordinates[0]};
				}
				border = flightModeBorder;
			}
			else
			{
				terrainCenter = Coordinates;
				border = centerBorder;
			}

			CenterArea = LongLatUtility.CalcBoundingBox(terrainCenter);
			CenterArea = LongLatUtility.ExpandBoundingBox(CenterArea, border);
			CenterArea = LongLatUtility.SetMinBoundingBox(CenterArea, minCenterSize);
			CenterArea = LongLatUtility.MakeSquare(CenterArea);
			var center = CenterArea.Center;
			Debug.Log("Center Area: " + CenterArea);

			Path = Coordinates.Select(x => x.GetPosition(CenterArea)).ToArray();

			//Notify other objects that the path and the coordinates were loaded
			if (CoordinatesLoaded != null)
			{
				CoordinatesLoaded(Coordinates);
			}

			//Start loading color maps
			double toSouth = CenterArea.south - center.latitude;
			double toNorth = CenterArea.north - center.latitude;
			double toWest = LongLatUtility.ToRealAngle(CenterArea.west - center.longitude);
			double toEast = LongLatUtility.ToRealAngle(CenterArea.east - center.longitude);

			var coroutines = new List<Coroutine>();
			var lodAreaLists = new List<List<LodArea>>();
			for (int i = 0; i < lodLevels.Length; i++)
			{
				//The first (after the center) LOD level has to be split in individual textures.
				var list = new List<LodArea>();
				lodAreaLists.Add(list);
				coroutines.Add(
					StartCoroutine(LoadColorMap(i, center, toSouth, toNorth, toWest, toEast, i > 0 && i <= textureSplitLOD, list)));
			}

			//Wait for color maps to finish loading
			foreach (var c in coroutines)
			{
				yield return c;
			}
			coroutines.Clear();

			//Add all color maps to the LODAreas list
			foreach (var list in lodAreaLists)
			{
				LODAreas.AddRange(list);
			}

			//Start downloading the height maps
			for (int i = 0; i < LODAreas.Count; i++)
			{
				coroutines.Add(StartCoroutine(LoadHeightMaps(LODAreas[i])));
			}

			//Wait until all height maps have finished downloading
			foreach (var c in coroutines)
			{
				yield return c;
			}
			coroutines.Clear();

			//Notify other objects that all textures have been loaded 
			if (MapLoaded != null)
			{
				MapLoaded(isReloadingRoute);
			}

#if UNITY_EDITOR && COLLECT_STATS
			var endTime = Time.realtimeSinceStartup;

			var statsWriter = new StreamWriter(dataCollection + "info.txt");

			statsWriter.Write("Path Length: ");
			float length = 0;
			for (int i = 0; i < Path.Length - 1; i++)
			{
				var cur = Path[i];
				var next = Path[i + 1];

				length += Vector3.Distance(cur, next);
			}
			statsWriter.WriteLine(length);
			statsWriter.WriteLine();

			var centerSize = LongLatUtility.CalcMeterSize(CenterArea);

			statsWriter.Write("Center Area Size: ");
			statsWriter.WriteLine(centerSize);

			for (var i = 0; i < lodAreas.Count; i++)
			{
				var area = lodAreas[i];
				var size = LongLatUtility.CalcMeterSize(area.bounds);

				statsWriter.Write("Bounding Box ");
				statsWriter.Write(i);
				statsWriter.Write(" Size: ");
				statsWriter.WriteLine(size);
			}

			statsWriter.WriteLine();

			statsWriter.Write("Color Texture Bytes: ");
			statsWriter.WriteLine(colorMapBytes);

			statsWriter.Write("Height Texture Bytes: ");
			statsWriter.WriteLine(heightMapBytes);

			statsWriter.WriteLine();

			statsWriter.Write("Terrain Loading and Generation Time: ");
			statsWriter.WriteLine(endTime - startTime);
			statsWriter.Close();
	#endif
		}
		finally
		{
			//In case this is reached because of an error, abort loading.
			StopAllCoroutines();
			IsLoadingTextures = false;
		}
	}

	private void Clear()
	{
		foreach (var area in LODAreas)
		{
			foreach (var heightMap in area.heightMaps)
			{
				DestroyImmediate(heightMap);
			}
		}

		LODAreas.Clear();
	}

	public void ClearAll()
	{
		StopAllCoroutines();
		IsLoadingTextures = false;

		Clear();
		Coordinates = new GeoCoordinate[0];
		Path = new Vector3[0];
		CenterArea = new GeoBoundingBox();

		IsMapLoaded = false;
	}

	private IEnumerator LoadCoordinates()
	{
		string name;
		string data;

		if (offlineMode)
		{
			name = offlineRouteName.ToLower();
			data = offlineData;
		}
		else
		{
			var coordinateLoader = new WWW(coordinateLink);
			yield return coordinateLoader;

			name = coordinateLink.ToLower();
			data = coordinateLoader.text;
		}

		if (name.EndsWith(".igc"))
		{
			Coordinates = IGCParser.Parse(data);
		}
		else if (name.EndsWith(".gpx"))
		{
			Coordinates = GPXParser.Parse(data);
		}
		else if (name.EndsWith(".kml"))
		{
			Coordinates = KMLParser.Parse(data);
		}
		else
		{
			Debug.LogError("Invalid coordinate link");
			Coordinates = new[]
			{
				new GeoCoordinate()
			};
		}
	}

	private IEnumerator LoadColorMap(int i, GeoCoordinate center, double toSouth, double toNorth, double toWest, double toEast, bool split, List<LodArea> currentLodAreas)
	{
		int pow;

		if (split)
		{
			pow = (int)Math.Pow(3, i-1);
		}
		else if (i == lodLevels.Length - 1)
		{
			//The last level must be at least the size of the view distance
			pow = (int)Math.Pow(3, TerrainUtility.CalcLODCount(CenterArea) + 1);
		}
		else
		{
			pow = (int)Math.Pow(3, i);
		}

		
		//Bing maps doesn't return the exact bounding box, so it's necessary to request a larger area to cover the whole area
		//At least that's what's happening with textures larger than 1024, which is why I don't use those any more
		double longitudeMeter = LongLatUtility.CalcMaxLongitudeDegreeMeter(CenterArea);
		double latitudeMeter = LongLatUtility.CalcMaxLatitudeDegreeMeter(CenterArea);

		double longLoadingBoundary = 1 + (textureLoadingBoundary - 1) * Math.Max(latitudeMeter / longitudeMeter, 1);
		double latLoadingBoundary = 1 + (textureLoadingBoundary - 1) * Math.Max(longitudeMeter / latitudeMeter, 1);

		//longLoadingBoundary = 1;
		//latLoadingBoundary = 1;

		double south = LongLatUtility.ToRealAngle(center.latitude + toSouth * pow * latLoadingBoundary);
		double north = LongLatUtility.ToRealAngle(center.latitude + toNorth * pow * latLoadingBoundary);
		double west = LongLatUtility.ToRealAngle(center.longitude + toWest * pow * longLoadingBoundary);
		double east = LongLatUtility.ToRealAngle(center.longitude + toEast * pow * longLoadingBoundary);
		var area = new GeoBoundingBox(south, west, north, east);

		//Make sure it's actually square. This will never make the area smaller.
		area = LongLatUtility.MakeSquare(area);

		//Start loading color map for the calculated area
		if (split)
		{
			var coroutines = new List<Coroutine>();

			double width = LongLatUtility.AngleToPositive(east - west);
			double length = north - south;

			for (int x = -1; x <= 1; x++)
			{
				for (int y = -1; y <= 1; y++)
				{
					if (x == 0 && y == 0)
					{
						continue;
					}

					var partialArea = area + new GeoCoordinate(width * x, length * y);

					var newArea = new LodArea();
					currentLodAreas.Add(newArea);
					coroutines.Add(StartCoroutine(LoadColorMap(partialArea, lodLevels[i], newArea)));
				}
			}

			foreach (var c in coroutines)
			{
				yield return c;
			}
		}
		else
		{
			var newArea = new LodArea();
			currentLodAreas.Add(newArea);
			yield return LoadColorMap(area, lodLevels[i], newArea);
		}
	}

	private IEnumerator LoadColorMap(GeoBoundingBox area, LodLevelSettings lodLevel, LodArea lodArea)
	{
		//Create bing map parameters
		var sb = new StringBuilder();
		sb.Append("http://dev.virtualearth.net/REST/v1/Imagery/Map/Aerial?");

		//CenterArea
		sb.Append("mapArea=");
		sb.Append(area);

		//Size
		sb.Append("&mapSize=");
		sb.Append(lodLevel.colorMapResolution);
		sb.Append(",");
		sb.Append(lodLevel.colorMapResolution);

		//Key
		sb.Append("&key=");
		sb.Append(bingApiKey);

		//Load metadata
		var metadataLoader = new WWW(sb + "&mapMetadata=1");
		yield return metadataLoader;
		var json = new JSONObject(metadataLoader.text);

		if (!string.IsNullOrEmpty(metadataLoader.error))
		{
			ShowError(metadataLoader.error);
		}

		var newBounds = new GeoBoundingBox();
		try
		{
			var boundingBox = json["resourceSets"][0]["resources"][0]["bbox"];
			newBounds.south = boundingBox[0].n;
			newBounds.west = boundingBox[1].n;
			newBounds.north = boundingBox[2].n;
			newBounds.east = boundingBox[3].n;
		}
		catch (Exception e)
		{
			Debug.LogError(e);
		}

		lodArea.bounds = newBounds;
		lodArea.lodLevel = lodLevel;

		//Load image
		var imageLoader = new WWW(sb.ToString());
		yield return imageLoader;

#if UNITY_EDITOR && COLLECT_STATS
		colorMapBytes += imageLoader.bytesDownloaded;
		File.WriteAllBytes(dataCollection + area + ".png", imageLoader.texture.EncodeToPNG());
#endif

		lodArea.colorMap = imageLoader.texture;

		if (!string.IsNullOrEmpty(imageLoader.error))
		{
			ShowError(imageLoader.error);
		}
	}

	private void ShowError(string imageLoaderError)
	{
		if (errorMessage.activeSelf)
		{
			return;
		}

		errorMessage.SetActive(true);
		errorMessageText.text = errorMessageString + imageLoaderError;
	}

	private IEnumerator LoadHeightMaps(LodArea area)
	{
		//Find a zoom level that has less then the specified max tile value
		int zoomLevel = maxHeightmapZoom + 1;
		do
		{
			zoomLevel--;

			area.tileStart = LongLatUtility.CalcTilePosition(area.bounds.west, area.bounds.north, zoomLevel);
			area.tileEnd = LongLatUtility.CalcTilePosition(area.bounds.east, area.bounds.south, zoomLevel);
			area.worldTileCount = LongLatUtility.GetTileCount(zoomLevel);
		}
		while (CalcAreaTileCount(area.tileStart, area.tileEnd, area.worldTileCount) > Math.Max(area.lodLevel.maxHeightmapTiles, 1));

		area.zoomLevel = zoomLevel;

		//Create texture array
		area.heightMaps = new TextureGrid(
			Mathd.ModularDistance(area.tileStart.TileX, area.tileEnd.TileX, area.worldTileCount) + 1,
			Mathd.ModularDistance(area.tileStart.TileY, area.tileEnd.TileY, area.worldTileCount) + 1);

		//Calculate border coordinates, with modulo operators
		int startX, endX, startY, endY;
		if (area.tileEnd.TileX - area.tileStart.TileX <= Mathd.Mod(area.tileStart.TileX - area.tileEnd.TileX, area.worldTileCount))
		{
			startX = area.tileStart.TileX;
			endX = area.tileEnd.TileX;
		}
		else
		{
			startX = area.tileEnd.TileX;
			endX = area.tileStart.TileX + area.worldTileCount;
		}
		if (area.tileEnd.TileY - area.tileStart.TileY <= Mathd.Mod(area.tileStart.TileY - area.tileEnd.TileY, area.worldTileCount))
		{
			startY = area.tileStart.TileY;
			endY = area.tileEnd.TileY;
		}
		else
		{
			startY = area.tileEnd.TileY;
			endY = area.tileStart.TileY + area.worldTileCount;
		}

		area.heightmapStartX = startX;
		area.heightmapStartY = startY;

		//Load heightmaps
		string mapzenFormat = "https://tile.mapzen.com/mapzen/terrain/v1/terrarium/{0}/{1}/{2}.png?api_key=" + mapzenKey;

		for (int rawX = startX, tX = 0; rawX <= endX; rawX++, tX++)
		{
			int x = Mathd.Mod(rawX, area.worldTileCount);
			for (int rawY = startY, tY = 0; rawY <= endY; rawY++, tY++)
			{
				int y = Mathd.Mod(rawY, area.worldTileCount);

				string link = string.Format(mapzenFormat, zoomLevel, x, y);
				//Debug.Log(link);
				var imageLoader = new WWW(link);
				yield return imageLoader;

#if UNITY_EDITOR && COLLECT_STATS
				heightMapBytes += imageLoader.bytesDownloaded;
				File.WriteAllBytes(dataCollection + string.Format("{0}-{1}-{2}.png", zoomLevel, x, y), imageLoader.texture.EncodeToPNG());
#endif

				if (!string.IsNullOrEmpty(imageLoader.error))
				{
					ShowError(imageLoader.error);
				}

				area.heightMaps[tX, tY] = imageLoader.texture;
				area.heightMaps[tX, tY].wrapMode = TextureWrapMode.Clamp;
			}
		}
	}

	private int CalcAreaTileCount(GeoTileCoordinate start, GeoTileCoordinate end, int tileCount)
	{
		return (Mathd.ModularDistance(start.TileX, end.TileX, tileCount) + 1) *
			   (Mathd.ModularDistance(start.TileY, end.TileY, tileCount) + 1);
	}

	[Serializable]
	public class LodLevelSettings
	{
		public int colorMapResolution;
		public int maxHeightmapTiles;
	}
}