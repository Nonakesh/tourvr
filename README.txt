Requirements:
At least Unity 5.6.0 (For Native Google Cardboard)

All important scripts are in the folder Assets/Scripts.

There is only one scene (MainMenu), the terrain will be loaded in this scene.

To automatically download offline data, activate the BatchSaver object in the scene view.
The track specified in it's settings will be saved in the folder Assets/OfflineData.
To load a offline track, put the newly created "Data" object and use it on the LoadSampleRoute button,
which can be found in the scene view, MainMenu/LoadSampleRoute.
It would be possible to have multiple sample routes by duplicating this button and setting different
data objects.