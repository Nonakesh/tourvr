﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class GraphicsLevelContainer : MonoBehaviour
{
	public int defaultLevel;
	public GraphicsLevel[] levels;

	public Text levelName;

	[Header("Setting Changers")]
	public ValueChanger lodCount;
	public ValueChanger textureSize;
	public ValueChanger heightMapCount;
	public ValueChanger subdivisions;
	public ValueChanger viewDistance;

	private void Start()
	{
		if (!PlayerPrefs.HasKey("GraphicsLevel"))
		{
			SetLevel(defaultLevel);
			PlayerPrefs.SetInt("GraphicsLevel", defaultLevel);
			PlayerPrefs.Save();
		}

		UpdateName();
	}

	private void SetLevel(int i)
	{
		var level = levels[i];

		lodCount.SetValue(level.lodCount);
		textureSize.SetValue(level.textureSize);
		heightMapCount.SetValue(level.heightMapCount);
		subdivisions.SetValue(level.subdivisions);
		viewDistance.SetValue(level.viewDistance);

		UpdateName();
	}

	public void Next()
	{
		int level = PlayerPrefs.GetInt("GraphicsLevel");
		level++;

		if (level >= levels.Length)
		{
			level = 0;
		}

		PlayerPrefs.SetInt("GraphicsLevel", level);
		PlayerPrefs.Save();

		SetLevel(level);
	}

	public void Prev()
	{
		int level = PlayerPrefs.GetInt("GraphicsLevel");
		level--;

		if (level < 0)
		{
			level = levels.Length - 1;
		}

		PlayerPrefs.SetInt("GraphicsLevel", level);
		PlayerPrefs.Save();

		SetLevel(level);
	}

	private void UpdateName()
	{
		levelName.text = levels[PlayerPrefs.GetInt("GraphicsLevel")].name;
	}
}

[Serializable]
public struct GraphicsLevel
{
	public string name;
	public int viewDistance;
	public int lodCount;
	public int textureSize;
	public int heightMapCount;
	public int subdivisions;
}
