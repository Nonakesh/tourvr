﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

[RequireComponent(typeof (MapLoader))]
public class TerrainGenerator : MonoBehaviour
{
	public int highDetailArea = 2;
	public Material terrainMaterial;

	private IMapData map;

	private List<int> subdivisions = new List<int> { 128 };
	private List<GameObject> objs = new List<GameObject>();
	private List<GameObject> oldObjs;

	public event Action<bool> TerrainGenerated;

	public bool IsGeneratingTerrain { get; private set; }
	public bool IsReloading { get; private set; }

	private void Start()
	{
		//map = GetComponent<MapLoader>();
		//map.MapLoaded += () => StartCoroutine(GenerateTerrain());
	}

	public void SetMapLoader(IMapData loader)
	{
		if (map != null)
		{
			map.MapLoaded -= OnMapLoaded;
		}

		map = loader;
		map.MapLoaded += OnMapLoaded;
	}

	private void OnMapLoaded(bool isReloading)
	{
		StartCoroutine(GenerateTerrain(isReloading));
	}

	private IEnumerator GenerateTerrain(bool isReloading)
	{
		try
		{
			IsReloading = isReloading;
			IsGeneratingTerrain = true;

			oldObjs = objs;
			objs = new List<GameObject>();

			highDetailArea = PlayerPrefs.GetInt("HighResLODs");
			subdivisions = new List<int>(highDetailArea + 1);
			for (int i = 0; i < highDetailArea; i++)
			{
				subdivisions.Add(PlayerPrefs.GetInt("HighResSubdivisions"));
			}
			subdivisions.Add(PlayerPrefs.GetInt("Subdivisions"));

			GeoBoundingBox centerBounds = map.CenterArea;

			//Generate terrain
			GenerateLodLevel(centerBounds, centerBounds, 0, 0, 0, true);

			GeoCoordinate origin = new GeoCoordinate(centerBounds.west, centerBounds.south);
			for (int i = 0; i <= TerrainUtility.CalcLODCount(map.CenterArea); i++)
			{
				double longSize = centerBounds.LongitudeSize * Math.Pow(3, i);
				double latSize = centerBounds.LatitudeSize * Math.Pow(3, i);

				for (int x = -1; x <= 1; x++)
				{
					for (int y = -1; y <= 1; y++)
					{
						if (x == 0 && y == 0)
						{
							continue;
						}

						var bounds = new GeoBoundingBox();
						bounds.west = LongLatUtility.ToRealAngle(origin.longitude + x * longSize);
						bounds.east = LongLatUtility.ToRealAngle(origin.longitude + (x + 1) * longSize);
						bounds.south = origin.latitude + y * latSize;
						bounds.north = origin.latitude + (y + 1) * latSize;

						GenerateLodLevel(bounds, centerBounds, i, x, y, false);
						yield return null;
					}
				}

				origin = new GeoCoordinate(LongLatUtility.ToRealAngle(origin.longitude - longSize), origin.latitude - latSize);
			}

			//Swap old for new terrain
			if (oldObjs != null)
			{
				foreach (var obj in oldObjs)
				{
					CleanObjectData(obj);

					Destroy(obj);
				}
				oldObjs = null;

				foreach (var obj in objs)
				{
					obj.SetActive(true);
				}
			}

			if (TerrainGenerated != null)
			{
				TerrainGenerated(isReloading);
			}
		}
		finally
		{
			StopAllCoroutines();
			IsGeneratingTerrain = false;
		}
	}

	private void GenerateLodLevel(GeoBoundingBox bounds, GeoBoundingBox centerBounds, int lod, int tileX, int tileY, bool generateCollider)
	{
		//Always use the meter-size of the center, to avoid visual artifacts at the cost of accuracy
		double longMeter = LongLatUtility.CalcMaxLongitudeDegreeMeter(centerBounds);
		double latMeter = LongLatUtility.CalcMaxLatitudeDegreeMeter(centerBounds);

		double offsetX = LongLatUtility.ToRealAngle(bounds.west - centerBounds.west) * longMeter;
		double offsetY = LongLatUtility.ToRealAngle(bounds.south - centerBounds.south) * latMeter;

		double sizeX = Mathd.ModularDistance(bounds.east, bounds.west, 360) * longMeter;
		double sizeY = (bounds.north - bounds.south) * latMeter;

		var mesh = new Mesh();
		var vertices = new List<Vector3>();
		var uvs = new List<Vector2>();
		var triangles = new List<int>();

		//Vertices
		for (int x = 0; x <= GetSubdivisions(lod); x++)
		{
			for (int y = 0; y <= GetSubdivisions(lod); y++)
			{
				float uvX = (float)x / GetSubdivisions(lod);
				float uvY = (float)y / GetSubdivisions(lod);

				uvs.Add(new Vector2(uvX, uvY));

				float height = CalcHeight(lod, tileX, tileY, x, y, bounds);
				vertices.Add(new Vector3((float)(uvX * sizeX), height, (float)(uvY * sizeY)));
			}
		}

		//Triangles
		int length = GetSubdivisions(lod) + 1;
		for (int x = 0; x < length - 1; x++)
		{
			for (int y = 0; y < length - 1; y++)
			{
				triangles.Add(x * length + y);
				triangles.Add(x * length + (y + 1));
				triangles.Add((x + 1) * length + y);

				triangles.Add(x * length + (y + 1));
				triangles.Add((x + 1) * length + (y + 1));
				triangles.Add((x + 1) * length + y);
			}
		}

		//Set variables
		mesh.SetVertices(vertices);
		mesh.SetUVs(0, uvs);
		mesh.SetTriangles(triangles, 0);
		mesh.RecalculateNormals();

		var obj = new GameObject("Terrain", typeof(MeshFilter), typeof(MeshRenderer));
		obj.transform.parent = transform;
		obj.transform.position = new Vector3((float) offsetX, 0, (float) offsetY);
		obj.GetComponent<MeshFilter>().sharedMesh = mesh;

		//Find current lod area and get texture
		LodArea area = map.LODAreas.FirstOrDefault(lodArea => LongLatUtility.IsInBoundingBox(lodArea.bounds, bounds));

		if (area == null)
		{
			Debug.LogError("No texture for current bounds found " + bounds);
			return;
		}

		double areaWidth = Mathd.Mod(area.bounds.east - area.bounds.west, 360.0);
		double areaLength = area.bounds.north - area.bounds.south;
		double boundsWidth = Mathd.Mod(bounds.east - bounds.west, 360.0);
		double boundsLength = bounds.north - bounds.south;
		double longOffset = Mathd.Mod(bounds.west - area.bounds.west, 360.0);
		double latOffset = bounds.south - area.bounds.south;
		double scaleX = (boundsWidth / areaWidth);
		double scaleY = (boundsLength / areaLength);
		double texOffsetX = longOffset / areaWidth;
		double texOffsetY = latOffset / areaLength;

		var mat = new Material(terrainMaterial);
		mat.mainTexture = area.colorMap;
		mat.mainTextureScale = new Vector2((float) scaleX, (float) scaleY);
		mat.mainTextureOffset = new Vector2((float)texOffsetX, (float)texOffsetY);
		obj.GetComponent<MeshRenderer>().sharedMaterial = mat;

		if (generateCollider)
		{
			obj.AddComponent<MeshCollider>().sharedMesh = mesh;
		}

		if (oldObjs != null)
		{
			obj.SetActive(false);
		}

		objs.Add(obj);
	}

	private float CalcHeight(int lod, int tileX, int tileY, int x, int y, GeoBoundingBox bounds)
	{
		int lodX = (tileX + 1) * GetSubdivisions(lod) + x;
		int lodY = (tileY + 1) * GetSubdivisions(lod) + y;

		int lodSubdivFactor = (GetSubdivisions(lod) / GetSubdivisions(lod + 1));
		int borderSmooth = 3 * lodSubdivFactor;

		if ((tileX < 0 && x == 0) || (tileX > 0 && x == GetSubdivisions(lod)))
		{
			float height0 = GetHeight(lod, x, y - lodY % borderSmooth, bounds);
			float height1 = GetHeight(lod, x, y - lodY % borderSmooth + borderSmooth, bounds);
			return Mathf.Lerp(height0, height1, (lodY % borderSmooth) / ((float)borderSmooth));
		}
		if ((tileY < 0 && y == 0) || (tileY > 0 && y == GetSubdivisions(lod)))
		{
			float height0 = GetHeight(lod, x - lodX % borderSmooth, y, bounds);
			float height1 = GetHeight(lod, x - lodX % borderSmooth + borderSmooth, y, bounds);
			return Mathf.Lerp(height0, height1, (lodX % borderSmooth) / ((float)borderSmooth));
		}

		return GetHeight(lod, x, y, bounds);
	}

	private float GetHeight(int lod, int x, int y, GeoBoundingBox bounds)
	{
		double longitude = Mathd.LerpAngle(bounds.west, bounds.east, (float)x / GetSubdivisions(lod));
		double latitude = Mathd.Lerp(bounds.south, bounds.north, (float)y / GetSubdivisions(lod));
		return TerrainUtility.GetHeightValue(map.LODAreas, longitude, latitude);
	}

	/// <summary>
	/// Returns the subdivisions for the chosen lod. The last lod is repeated.
	/// </summary>
	/// <param name="lod"></param>
	/// <returns></returns>
	private int GetSubdivisions(int lod)
	{
		if (lod >= subdivisions.Count)
		{
			return subdivisions[subdivisions.Count - 1];
		}

		return subdivisions[lod];
	}

	public void Clear()
	{
		StopAllCoroutines();
		IsGeneratingTerrain = false;

		foreach (var obj in objs)
		{
			CleanObjectData(obj);

			Destroy(obj);
		}
		objs.Clear();

		if (oldObjs != null)
		{
			foreach (var obj in oldObjs)
			{
				CleanObjectData(obj);

				Destroy(obj);
			}
		}
		oldObjs = null;
	}

	private void CleanObjectData(GameObject obj)
	{
		var mesh = obj.GetComponent<MeshFilter>().sharedMesh;
		DestroyImmediate(mesh);

		if (map is MapLoader)
		{
			var tex = obj.GetComponent<Renderer>().sharedMaterial.mainTexture;
			DestroyImmediate(tex);
		}
	}
}
