﻿using System;
using UnityEngine;
using System.Collections;
using System.Linq;
using UnityEngine.UI;

public class PlayerPrefAlternativeSelector : MonoBehaviour
{
	public string settingName;
	public ImageValue[] imageValues;

	public Color activeColor = Color.green;
	public Color inactiveColor = Color.gray;

	private int value;

	private void Start()
	{
		string savedValue = PlayerPrefs.GetString(settingName);

		for (int i = 0; i < imageValues.Length; i++)
		{
			if (imageValues[i].value == savedValue)
			{
				value = i;
				break;
			}
		}

		UpdateButtonColors();
	}

	public void SetValue(int i)
	{
		value = i;

		PlayerPrefs.SetString(settingName, imageValues[i].value);
		PlayerPrefs.Save();

		UpdateButtonColors();
	}

	private void UpdateButtonColors()
	{
		for (int i = 0; i < imageValues.Length; i++)
		{
			var image = imageValues[i].image;

			image.color = i == value ? activeColor : inactiveColor;
		}
	}

	[Serializable]
	public class ImageValue
	{
		public Image image;
		public string value;
	}
}
