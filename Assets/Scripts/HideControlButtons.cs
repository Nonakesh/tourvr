﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HideControlButtons : MonoBehaviour
{
	public GameObject[] toggleableButtons;

	public Text text;
	public string activeText = "Show Buttons";
	public string inactiveText = "Hide Buttons";

	private bool active = true;

	public void ToggleButtons()
	{
		active = !active;

		text.text = active ? activeText : inactiveText;

		foreach (var obj in toggleableButtons)
		{
			obj.SetActive(active);
		}
	}
}
