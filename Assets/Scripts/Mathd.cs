﻿using System;

public static class Mathd
{
	public static int Mod(int x, int m)
	{
		if (m < 0) m = -m;
		int r = x % m;
		return r < 0 ? r + m : r;
	}

	public static int ModularDistance(int a, int b, int m)
	{
		return Math.Min(Mod(a - b, m), Mod(b - a, m));
	}

	public static double Mod(double x, double m)
	{
		if (m < 0) m = -m;
		double r = x % m;
		return r < 0 ? r + m : r;
	}

	public static double ModularDistance(double a, double b, double m)
	{
		return Math.Min(Mod(a - b, m), Mod(b - a, m));
	}

	//Those are taken from a disassemly of Mathf. I think Repeat is basically the same as Modulo, but I'll leave it like they did it.
	public static double Repeat(double t, double length)
	{
		return t - Math.Floor(t / length) * length;
	}

	public static double Clamp01(double value)
	{
		if (value < 0.0)
			return 0.0f;
		if (value > 1.0)
			return 1f;
		return value;
	}

	public static double Lerp(double a, double b, double t)
	{
		return a + (b - a) * Clamp01(t);
	}

	public static double LerpAngle(double a, double b, double t)
	{
		double num = Repeat(b - a, 360.0);
		if (num > 180.0)
			num -= 360.0;
		return a + num * Clamp01(t);
	}
}