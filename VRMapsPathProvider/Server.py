from http.server import HTTPServer, SimpleHTTPRequestHandler, BaseHTTPRequestHandler
import os

DEFAULT_PATH = "Paths"

class myHandler(BaseHTTPRequestHandler):
    def do_HEAD(s):
        s.send_response(200)
        s.send_header("Content-type", "text/plain")
        s.end_headers()

    def do_GET(self):
        """Respond to a GET request."""
        self.send_response(200)
        self.send_header("Content-type", "text/plain")
        self.end_headers()

        if self.path is "" or self.path is "/":
            for file in os.listdir(DEFAULT_PATH):
                self.wfile.write(bytes(file + "\n", "utf-8"))
        else:
            #TODO: Fix this, so the user can't access other folders
            try:
                with open(DEFAULT_PATH + self.path, mode='rb') as file: # b is important -> binary
                    fileContent = file.read()
                    self.wfile.write(fileContent)
            except:
                self.wfile.write(bytes("File could not be read", "utf-8"))


def run_server(server_class=HTTPServer, handler_class=SimpleHTTPRequestHandler, port=8000):
    server_address = ('', port)
    httpd = server_class(server_address, handler_class)

    print("Server starting")
    httpd.serve_forever()

if __name__ == "__main__":
    run_server(handler_class=myHandler)