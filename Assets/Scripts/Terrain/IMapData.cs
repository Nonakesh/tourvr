﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public interface IMapData
{
	GeoBoundingBox CenterArea { get; }
	List<LodArea> LODAreas { get; }
	bool IsLoadingTextures { get; }
	GeoCoordinate[] Coordinates { get; }
	bool IsMapLoaded { get; }
	bool IsReloading { get; }
	Vector3[] Path { get; }

	event Action<bool> MapLoaded;

	void LoadTerrain(bool useFlightMode, bool isReloadingRoute, Vector3 transformPosition);
	void ClearAll();
}
