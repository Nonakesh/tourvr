﻿using UnityEngine;
using System.Collections;

public class ResetIP : VRInteractor
{
	public GameObject currentMenu;
	public GameObject nextMenu;

	protected override void OnSelectionCompleted()
	{
		PlayerPrefs.SetString("ServerIP", "");
		PlayerPrefs.Save();

		nextMenu.SetActive(true);
		currentMenu.SetActive(false);
	}
}
