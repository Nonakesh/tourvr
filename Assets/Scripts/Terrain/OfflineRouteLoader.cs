﻿using System;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Uses offline data to load the terrain
/// </summary>
public class OfflineRouteLoader : MonoBehaviour
{
	public bool isFlightRoute = false;

	public OfflineLoaderCollection offlineRoute;
	public GameObject currentRoot;
	public LoadingScreen loadingScreen;
	public CameraController controller;
	public GameObject mapLoaderParent;
	public TerrainGenerator generator;
	public MapExit exit;

	public void OnEnable()
	{
		generator.TerrainGenerated -= offlineRoute.LoadingFinished;
		generator.TerrainGenerated += offlineRoute.LoadingFinished;
	}

	//public void OnDisable()
	//{
	//	generator.TerrainGenerated -= offlineRoute.LoadingFinished;
	//}

	public void LoadRoute()
	{
		controller.FlightMode = isFlightRoute;

		exit.mapLoader = offlineRoute;
		generator.SetMapLoader(offlineRoute);
		controller.mapLoader = offlineRoute;

		mapLoaderParent.SetActive(true);

		loadingScreen.gameObject.SetActive(true);
		loadingScreen.StartLoading();

		currentRoot.SetActive(false);

		offlineRoute.LoadMap();
	}
}
