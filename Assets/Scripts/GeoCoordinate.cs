﻿using System;
using System.Globalization;
using UnityEngine;

[Serializable]
public struct GeoCoordinate
{
	public double latitude, longitude;
	public float height;

	/// <summary>
	/// Time in seconds
	/// </summary>
	public long time;

	public GeoCoordinate(double longitude, double latitude) : this()
	{
		this.latitude = latitude;
		this.longitude = longitude;
	}

	public Vector3 GetPosition(GeoBoundingBox area)
	{
		double longOffset = LongLatUtility.ToRealAngle(longitude - area.west);
		double latOffset = latitude - area.south;

		double maxLongMeter = LongLatUtility.CalcMaxLongitudeDegreeMeter(area);
		double maxLatMeter = LongLatUtility.CalcMaxLatitudeDegreeMeter(area);

		return new Vector3((float) (longOffset * maxLongMeter), height, (float) (latOffset * maxLatMeter));
	}

	public static GeoCoordinate FromPosition(Vector3 pos, GeoBoundingBox area)
	{
		double maxLongMeter = Math.Max(LongLatUtility.CalcLongitudeDegreeMeters(area.south),
			LongLatUtility.CalcLongitudeDegreeMeters(area.north));
		double maxLatMeter = Math.Max(LongLatUtility.CalcLatitudeDegreeMeters(area.south),
			LongLatUtility.CalcLatitudeDegreeMeters(area.north));

		return new GeoCoordinate(LongLatUtility.ToRealAngle(pos.x / maxLongMeter + area.west), pos.z / maxLatMeter + area.south);
	}

	public override string ToString()
	{
		return string.Format("Latitude: {0}, Longitude: {1}, Height: {2}, Time: {3}", latitude, longitude, height, time);
	}
}

[Serializable]
public struct GeoBoundingBox
{
	public double south, west, north, east;

	public GeoBoundingBox(double south, double west, double north, double east)
	{
		this.south = south;
		this.west = west;
		this.north = north;
		this.east = east;
	}

	public GeoCoordinate Start
	{
		get
		{
			return new GeoCoordinate(west, south);
		}
	}

	public GeoCoordinate End
	{
		get
		{
			return new GeoCoordinate(east, north);
		}
	}

	public GeoCoordinate Center
	{
		get
		{
			var center = new GeoCoordinate();

			if (west < east)
			{
				center.longitude = (west + east) * 0.5;
			}
			else
			{
				center.longitude = LongLatUtility.ToRealAngle((west + east + 360) * 0.5);
			}

			center.latitude = (south + north) * 0.5;

			return center;
		}
	}

	public double LongitudeSize
	{
		get
		{
			if (west < east)
			{
				return east - west;
			}

			return east + 360 - west;
		}
	}

	public double LatitudeSize
	{
		get { return north - south; }
	}

	public override string ToString()
	{
		return string.Format(CultureInfo.InvariantCulture, "{0:F6},{1:F6},{2:F6},{3:F6}", south, west, north, east);
	}

	public GeoBoundingBox Scale(float scale)
	{
		var center = Center;
		double toWest = LongLatUtility.ToRealAngle(west - center.longitude);
		double toEast = LongLatUtility.ToRealAngle(east - center.longitude);
		double toSouth = south - center.latitude;
		double toNorth = north - center.latitude;

		toWest *= scale;
		toEast *= scale;
		toSouth *= scale;
		toNorth *= scale;

		return new GeoBoundingBox(center.latitude + toSouth, center.longitude + toWest, 
								  center.latitude + toNorth, center.longitude + toEast);
	}

	public static GeoBoundingBox operator +(GeoBoundingBox box, GeoCoordinate coord)
	{
		box.west = LongLatUtility.ToRealAngle(box.west + coord.longitude);
		box.east = LongLatUtility.ToRealAngle(box.east + coord.longitude);
		box.south = box.south + coord.latitude;
		box.north = box.north + coord.latitude;

		return box;
	}
}

[Serializable]
public struct GeoTileCoordinate
{
	public double x, y;

	public int TileX
	{
		get { return (int) x; }
	}

	public int TileY
	{
		get { return (int) y; }
	}

	public GeoTileCoordinate(double x, double y)
	{
		this.x = x;
		this.y = y;
	}
}