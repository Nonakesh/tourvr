﻿using UnityEngine;
using UnityEngine.UI;

public class DynamicSpeedChanger : MonoBehaviour
{
	public CameraController controller;

	public Text fastNeg, slowNeg, valueField, slowPos, fastPos;

	public Text pause;

	public float slowFlightValue = 0.25f;
	public float fastFlightValue = 1.0f;
	public float slowFloorValue = 1.0f;
	public float fastFloorValue = 10.0f;

	public float SlowValue
	{
		get { return controller.FlightMode ? slowFlightValue : slowFloorValue; }
	}

	public float FastValue
	{
		get { return controller.FlightMode ? fastFlightValue : fastFloorValue; }
	}

	public float SpeedValue
	{
		set
		{
			if (controller.FlightMode)
			{
				controller.flightSpeedModifier = value;
			}
			else
			{
				controller.autoWalkSpeed = value / 3.6f;
			}

			valueField.text = GetValueText(value);
		}
		get
		{
			if (controller.FlightMode)
			{
				return controller.flightSpeedModifier;
			}
			else
			{
				return controller.autoWalkSpeed * 3.6f;
			}
		}
	}

	private string GetValueText(float value)
	{
		if (controller.FlightMode)
		{
			return "x" + value.ToString("0.##");
		}
		else
		{
			return Mathf.RoundToInt(value) + " km/h";
		}
	}

	private float pauseValue;
	private bool paused;

	public void Awake()
	{
		controller.terrain.TerrainGenerated += isRouteReloaded =>
		{
			//In route reloading, not a new route.
			if (isRouteReloaded)
			{
				return;
			}

			if (controller.FlightMode)
			{
				fastNeg.text = (-fastFlightValue).ToString("##.##");
				slowNeg.text = (-slowFlightValue).ToString("##.##");
				slowPos.text = slowFlightValue.ToString("##.##");
				fastPos.text = fastFlightValue.ToString("##.##");
			}
			else
			{
				fastNeg.text = (-fastFloorValue).ToString("##.##");
				slowNeg.text = (-slowFloorValue).ToString("##.##");
				slowPos.text = slowFloorValue.ToString("##.##");
				fastPos.text = fastFloorValue.ToString("##.##");
			}

			ResetValue();
		};
	}

	public void SlowChange(bool reverse)
	{
		Unpause();
		AddValue(SlowValue * (reverse ? -1 : 1));
	}

	public void FastChange(bool reverse)
	{
		Unpause();
		AddValue(FastValue * (reverse ? -1 : 1));
	}

	public void ResetValue()
	{
		Unpause();

		SpeedValue = PlayerPrefs.GetInt(controller.FlightMode ? "FlightspeedMultiplier" : "AutoWalkSpeed");
	}

	public void TogglePause()
	{
		if (paused)
		{
			Unpause();
		}
		else
		{
			Pause();
		}
	}

	public void Pause()
	{
		if (!paused)
		{
			paused = true;
			pauseValue = SpeedValue;
			SpeedValue = 0;
			pause.text = "Play";
			valueField.text = GetValueText(pauseValue);
		}
	}

	public void Unpause()
	{
		if (paused)
		{
			paused = false;
			SpeedValue = pauseValue;
			pause.text = "Pause";
		}
	}

	public void AddValue(float value)
	{
		Unpause();

		SpeedValue = Mathf.Max(SpeedValue + value, 0);
	}
}
