﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Random = UnityEngine.Random;

public class CameraController : MonoBehaviour
{
	public IMapData mapLoader;
	public TerrainGenerator terrain;

	public Transform forwardButton;
	public Transform backwardButton;
	public Transform buttonRoot;
	public GameObject speedChangeButtons;
	public GameObject hideButton;

	public float coordinateMergeDistance = 0.1f;
	public bool FlightMode { get; set; }

	public float skipTime = 30;

	public CatmullRomCurves.CatmullRomType flightCurveType = CatmullRomCurves.CatmullRomType.Centripetal;
	public float flightModeReloadBorder = 0.8f;
	public float tangentCalculationDistance = 0.01f;
	[HideInInspector]
	public float flightSpeedModifier = 1;
	private int currentSplineIndex;
	private float currentSplineSegmentProgress;

	private bool useTeleportMovement = true;
	private float teleportDistance = 10;
	[HideInInspector]
	public float autoWalkSpeed;

	public float maxHorizonRotation = 5;

	private bool rotateView = true;
	private bool rotateFlightView = true;
	private bool rotateFlightHorizon = true;
	private float rotateHorizonTilt = 30;

	private float pathPosition = 0;

	private float heightOffset;

	public float horizonRotationSmoothing = 1;
	private float currentHorizonRotation = 0;
	private float currentHorizonRotationVelocity = 0;

	private IList<Vector3> path;
	private GeoBoundingBox centerArea;

	/// <summary>
	/// Start is executed when the object is created.
	/// </summary>
	private void Start()
	{
		terrain.TerrainGenerated += isReloadingRoute =>
		{
			//Reset camera controller
			if (!isReloadingRoute)
			{
				pathPosition = 0;
				currentSplineSegmentProgress = 0;
				currentSplineIndex = 0;

				LoadSettings();
			}

			if (FlightMode)
			{
				//Prepare data for flight mode
				var realPos = mapLoader.Path[0];
				var terrainPos = FitToTerrain(realPos);

				if (realPos.y < terrainPos.y)
				{
					heightOffset = terrainPos.y - realPos.y;
				}
				else
				{
					heightOffset = 0;
				}

				transform.position = realPos + Vector3.up * heightOffset;

				path = CatmullRomCurves.ExtrapolateEndPoints(mapLoader.Path);
				centerArea = mapLoader.CenterArea;

				forwardButton.gameObject.SetActive(false);
				backwardButton.gameObject.SetActive(false);
				speedChangeButtons.SetActive(true);
			}
			else
			{
				//Prepare hiking mode
				speedChangeButtons.SetActive(!useTeleportMovement);

				path = mapLoader.Path;

				TeleportToPoint(0);
			}
		};
	}

	private List<Vector3> ReducePath(Vector3[] originalPath)
	{
		var path = new List<Vector3>(originalPath);

		int i = 0;
		while (i < path.Count)
		{
			//If there are multiple path points at the same place, ignore them.
			while (i + 2 < path.Count && 
				   Vector3.Distance(path[i], path[i + 1]) < coordinateMergeDistance &&
				   Vector3.Distance(path[i], path[i + 2]) < coordinateMergeDistance)
			{
				path.RemoveAt(i+1);
			}

			i++;
		}

		return path;
	}

	private void LoadSettings()
	{
		flightSpeedModifier = PlayerPrefs.GetInt("FlightspeedMultiplier");

		useTeleportMovement = PlayerPrefs.GetString("WalkMode") == "Teleport";
		teleportDistance = PlayerPrefs.GetInt("TeleportDistance");
		autoWalkSpeed = PlayerPrefs.GetInt("AutoWalkSpeed") / 3.6f;

		rotateView = PlayerPrefs.GetString("RotateCameraWithPath") == "On";
		rotateFlightView = PlayerPrefs.GetString("RotateCameraInFlight") == "On";
		rotateFlightHorizon = PlayerPrefs.GetString("RotateHorizonInFlight") == "On";
		rotateHorizonTilt = PlayerPrefs.GetInt("MaxHorizonRotation");
	}

	private void Update()
	{
		//If the map it not loaded, or the terrain is not generated, don't move the camera yet.
		if (mapLoader == null || !mapLoader.IsMapLoaded || terrain.IsGeneratingTerrain && !terrain.IsReloading)
		{
			return;
		}

		//Flight mode or button
		if (FlightMode || !useTeleportMovement)
		{
			FollowSpinePath();
		}

		UpdateButtonPositions();
	}

	/// <summary>
	/// Puts all buttons at the right position, for example, forward and backward buttons should be in direction of travel.
	/// </summary>
	private void UpdateButtonPositions()
	{
		if (buttonRoot == null)
		{
			return;
		}

		buttonRoot.position = transform.position;
		buttonRoot.rotation = transform.rotation;

		if (!useTeleportMovement || FlightMode)
		{
			//No forward or backward buttons in Flight or AutoMove modes
			forwardButton.gameObject.SetActive(false);
			backwardButton.gameObject.SetActive(false);

			speedChangeButtons.SetActive(true);

			return;
		}
		else
		{
			//No speed changer in teleport mode
			speedChangeButtons.SetActive(false);
		}

		//Find the positions for the teleport buttons (in direction of travel)
		var nextPoint = GetPathPosition(NextPointAlongPath(pathPosition, teleportDistance));
		var prevPoint = GetPathPosition(NextPointAlongPath(pathPosition, -teleportDistance));

		var nextDir = nextPoint - transform.position;
		nextDir.y = 0;

		var prevDir = prevPoint - transform.position;
		prevDir.y = 0;

		if (nextDir.magnitude > 0.1f)
		{
			forwardButton.rotation = Quaternion.LookRotation(nextDir);
			forwardButton.gameObject.SetActive(true);
		}
		else
		{
			forwardButton.gameObject.SetActive(false);
		}

		if (prevDir.magnitude > 0.1f)
		{
			backwardButton.rotation = Quaternion.LookRotation(prevDir);
			backwardButton.gameObject.SetActive(true);
		}
		else
		{
			backwardButton.gameObject.SetActive(false);
		}
	}

	private void FollowSpinePath()
	{
		//Calculate new position
		if (currentSplineSegmentProgress >= 1 || currentSplineSegmentProgress < 0)
		{
			currentSplineSegmentProgress = 0;
			currentSplineIndex++;
		}

		if (!FlightMode && currentSplineIndex >= path.Count - 3 || FlightMode && currentSplineIndex >= mapLoader.Coordinates.Length - 3)
		{
			currentSplineIndex = 0;
		}

		if (currentSplineIndex < 0)
		{
			currentSplineIndex = 0;
			currentSplineSegmentProgress = 0;
		}

		transform.position = CatmullRomCurves.Interpolate(path, currentSplineIndex, flightCurveType, currentSplineSegmentProgress);

		//Calculate tangent
		var tangentPos0 = CalcTangentPos(currentSplineSegmentProgress - tangentCalculationDistance);
		var tangentPos1 = CalcTangentPos(currentSplineSegmentProgress + tangentCalculationDistance);

		var dir = tangentPos1 - tangentPos0;
		
		//Rotate view
		if ((!FlightMode && rotateView) || (FlightMode && rotateFlightView))
		{
			float oldRot = transform.eulerAngles.y;

			if (!FlightMode)
			{
				dir.y = 0;
			}

			transform.rotation = Quaternion.LookRotation(dir, Vector3.up);

			//Rotate horizon in flight mode
			if (FlightMode && rotateFlightHorizon)
			{
				float newRot = transform.eulerAngles.y;
				float rotDiff = (float) LongLatUtility.ToRealAngle(newRot - oldRot) / Time.deltaTime;

				float normalizedRotation = (rotDiff / maxHorizonRotation) * 0.5f + 0.5f;

				float rotationValue = Mathf.SmoothStep(rotateHorizonTilt, -rotateHorizonTilt, normalizedRotation);

				currentHorizonRotation = Mathf.SmoothDamp(currentHorizonRotation, rotationValue, ref currentHorizonRotationVelocity,
					horizonRotationSmoothing);

				Vector3 euler = transform.eulerAngles;
				euler.z = currentHorizonRotation;
				transform.eulerAngles = euler;
			}
		}

		//Increase the progress for next frame
		if (FlightMode)
		{
			//In flight mode, just use the timing from the flight data
			var start = mapLoader.Coordinates[currentSplineIndex + 1];
			var end = mapLoader.Coordinates[currentSplineIndex + 2];
			float segmentTime = end.time - start.time;
			currentSplineSegmentProgress += Time.deltaTime * flightSpeedModifier / segmentTime;
		}
		else
		{
			//In auto mode, use the tangents to approximate the distance per unit
			float dist = dir.magnitude;
			float pathDist = tangentCalculationDistance * 2;
			float distancePerSplineUnit = dist / pathDist;
			float splineUnitsPerSecond = autoWalkSpeed / distancePerSplineUnit;

			currentSplineSegmentProgress += Time.deltaTime * splineUnitsPerSecond;
		}

		if (FlightMode)
		{
			//Set terrain height offset
			transform.position += Vector3.up * heightOffset;

			//If plane is outside of center area, load again
			if (!mapLoader.IsLoadingTextures &&
				!LongLatUtility.IsInBoundingBox(centerArea.Scale(flightModeReloadBorder), GeoCoordinate.FromPosition(transform.position, centerArea)))
			{
				mapLoader.LoadTerrain(true, true, transform.position);
			}
		}
		else
		{
			//Set camera height to fit to the terrain
			transform.position = FitToTerrain(transform.position);
		}
	}

	public void AddMovementTime(bool forward)
	{
		AddMovementTime(forward ? skipTime : -skipTime);
	}

	private void AddMovementTime(float time)
	{
		if (FlightMode)
		{
			int iterations = 0;
			do
			{
				//In flight mode, just use the timing from the flight data
				var start = mapLoader.Coordinates[currentSplineIndex + 1];
				var end = mapLoader.Coordinates[currentSplineIndex + 2];
				float segmentTime = end.time - start.time;

				float speed = flightSpeedModifier / segmentTime;
				currentSplineSegmentProgress += time * speed;

				if (currentSplineSegmentProgress < 0)
				{
					currentSplineIndex--;
					time = currentSplineSegmentProgress / speed;
					currentSplineSegmentProgress = 1;
				}
				else if (currentSplineSegmentProgress > 1)
				{
					currentSplineIndex++;
					time = (currentSplineSegmentProgress - 1) / speed;
					currentSplineSegmentProgress = 0;
				}

				iterations++;
			}
			while (Mathf.Abs(time) > 0.1f && currentSplineIndex >= 0 && currentSplineIndex <= mapLoader.Coordinates.Length - 3 && iterations < 1000);
		}
		else
		{
			//In auto mode, use the tangents to approximate the distance per unit
			var tangentPos0 = CalcTangentPos(currentSplineSegmentProgress - tangentCalculationDistance);
			var tangentPos1 = CalcTangentPos(currentSplineSegmentProgress + tangentCalculationDistance);

			float dist = Vector3.Distance(tangentPos1, tangentPos0);
			float pathDist = tangentCalculationDistance * 2;
			float distancePerSplineUnit = dist / pathDist;
			float splineUnitsPerSecond = autoWalkSpeed / distancePerSplineUnit;

			currentSplineSegmentProgress += time * splineUnitsPerSecond;

			int nonFractional = Mathf.FloorToInt(currentSplineSegmentProgress);
			currentSplineIndex += nonFractional;
			currentSplineSegmentProgress = currentSplineSegmentProgress - nonFractional;
		}
	}

	private Vector3 CalcTangentPos(float tangentSegmentPos)
	{
		int tangentIndex = currentSplineIndex;

		if (tangentSegmentPos >= 1)
		{
			if (tangentIndex < path.Count - 4)
			{
				tangentIndex += 1;
				tangentSegmentPos -= 1;
			}
			else
			{
				tangentSegmentPos = 1;
			}
		}
		else if (tangentSegmentPos < 0)
		{
			if (tangentIndex > 0)
			{
				tangentIndex -= 1;
				tangentSegmentPos += 1;
			}
			else
			{
				tangentSegmentPos = 0;
			}
		}

		return CatmullRomCurves.Interpolate(path, tangentIndex, flightCurveType, tangentSegmentPos);
	}

	public void MoveForward()
	{
		var pos = NextPointAlongPath(pathPosition, teleportDistance);
		TeleportToPoint(pos);
	}

	public void MoveBackward()
	{
		var pos = NextPointAlongPath(pathPosition, -teleportDistance);
		TeleportToPoint(pos);
	}

	public float NextPointAlongPath(float startPosition, float targetDistance)
	{
		float distanceLeft = Mathf.Abs(targetDistance);
		float currentPos = startPosition;
		while (distanceLeft > 0.1f)
		{
			int next = NextPoint(currentPos, targetDistance);

			Vector3 nextPoint = path[next];
			Vector3 pos = InterpolatedPos(currentPos);

			float distance = Vector3.Distance(pos, nextPoint);

			if (distanceLeft - distance < 0)
			{
				float percent = distanceLeft / distance;
				currentPos = currentPos * (1 - percent) + next * percent;
				distanceLeft = 0;
			}
			else if (next <= 0)
			{
				return 0;
			}
			else if (next >= path.Count - 1)
			{
				return path.Count - 1;
			}
			else
			{
				currentPos = next;
				distanceLeft -= distance;
			}
		}

		return currentPos;
	}

	public void TeleportToPoint(float index)
	{
		transform.position = GetPathPosition(index);
		pathPosition = index;

		//Rotating the view while teleporting can be disorienting
		//if (rotateView)
		//{
		//	Vector3 dir;
		//	if (index >= path.Count - 1)
		//	{
		//		dir = path[path.Count - 1] - path[path.Count-2];
		//	}
		//	else if (index <= 0)
		//	{
		//		dir = path[1] - path[0];
		//	}
		//	else
		//	{
		//		var start = path[Mathf.FloorToInt(index)];
		//		var end = path[Mathf.CeilToInt(index)];
		//		dir = end - start;
		//	}

		//	dir.y = 0;
		//	transform.rotation = Quaternion.LookRotation(dir);
		//}

		UpdateButtonPositions();
	}

	private Vector3 GetPathPosition(float index)
	{
		var pos = InterpolatedPos(index);

		return FitToTerrain(pos);
	}

	/// <summary>
	/// Returns the same position, but the height of the terrain is used instead of the original height.
	/// </summary>
	/// <param name="pos"></param>
	/// <returns></returns>
	private static Vector3 FitToTerrain(Vector3 pos)
	{
		var ray = new Ray(pos + Vector3.up * 10000, Vector3.down);
		RaycastHit hit;
		if (Physics.Raycast(ray, out hit))
		{
			return hit.point;
		}

		Debug.LogError("No terrain was hit. This should never happen.");
		return pos;
	}

	private int NextPoint(float currentPos, float direction)
	{
		int dir = Math.Sign(direction);

		if (dir > 0)
		{
			int currentIndex = Mathf.FloorToInt(currentPos);
			return Math.Min(currentIndex + 1, path.Count-1);
		}
		else
		{
			int currentIndex = Mathf.CeilToInt(currentPos);
			return Math.Max(currentIndex - 1, 0);
		}
	}

	private int PreviousPoint(float currentPos, float direction)
	{
		int dir = Math.Sign(direction);

		if (dir > 0)
		{
			return Mathf.FloorToInt(currentPos);
		}

		return Mathf.CeilToInt(currentPos);
	}

	private Vector3 InterpolatedPos(float index)
	{
		if (index < 0)
		{
			return path[0];
		}

		if (index >= path.Count - 1)
		{
			return path[path.Count - 1];
		}

		var start = path[Mathf.FloorToInt(index)];
		var end = path[Mathf.CeilToInt(index)];
		float fraction = index - Mathf.Floor(index);

		return start * (1 - fraction) + end * fraction;
	}
}
