﻿using UnityEngine;
using System.Collections;

public class LoadingScreen : MonoBehaviour
{
	public TerrainGenerator generator;

	public GameObject buttonRoot;

	private void OnEnable()
	{
		generator.TerrainGenerated += TerrainGenerated;
	}

	private void OnDisable()
	{
		generator.TerrainGenerated -= TerrainGenerated;
	}

	public void StartLoading()
	{
		buttonRoot.SetActive(false);
	}

	private void TerrainGenerated(bool isReloadingRoute)
	{
		buttonRoot.SetActive(true);
		gameObject.SetActive(false);
	}
}
