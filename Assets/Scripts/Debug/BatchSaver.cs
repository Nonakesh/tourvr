﻿using System.Collections.Generic;
using UnityEngine;

public class BatchSaver : MonoBehaviour
{
#if UNITY_EDITOR
	public string routeLink;

	public SimpleSettings settings;
	public GameObject[] deactivateGameObjects;
	public string filePath;

	public MapLoader loader;
	public TerrainGenerator generator;

	private int batchLoadProgress;
	private OfflineLoaderCollection offlineLoaderCollection;
	private bool flightMode;

	public void Awake()
	{
		generator.enabled = false;
	}

	public void Start()
	{
		foreach (var obj in deactivateGameObjects)
		{
			obj.SetActive(false);
		}

		loader.transform.parent = null;
		loader.gameObject.SetActive(true);
		loader.enabled = true;

		//Set loader info
		flightMode = routeLink.ToLower().EndsWith(".igc");
		loader.coordinateLink = routeLink;
		loader.offlineMode = false;

		batchLoadProgress = 0;
		offlineLoaderCollection = ScriptableObject.CreateInstance<OfflineLoaderCollection>();

		loader.MapLoaded += TerrainLoaded;

		LoadNextTerrain();
	}

	private void TerrainLoaded(bool isReloadingTerrain)
	{
		offlineLoaderCollection.Loaders.Add(new OfflineLoader
		{
			CenterArea = loader.CenterArea,
			LODAreas = new List<LodArea>(loader.LODAreas)
		});

		LoadNextTerrain();
	}

	private void LoadNextTerrain()
	{
		if (batchLoadProgress >= settings.Settings.Length)
		{
			SaveOfflineData();
			return;
		}

		settings.ApplySetting(batchLoadProgress);
		batchLoadProgress++;

		loader.LoadTerrain(flightMode, false);
	}

	private void SaveOfflineData()
	{
		offlineLoaderCollection.Coordinates = loader.Coordinates;
		offlineLoaderCollection.Path = loader.Path;

		OfflineLoaderCollection.CreateAsset(offlineLoaderCollection, filePath);

		Debug.Log("Finished saving offline data.");
	}
#endif
}
