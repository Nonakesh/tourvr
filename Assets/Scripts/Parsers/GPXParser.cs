﻿using System.Linq;
using System.Xml.Linq;
using UnityEngine;

public static class GPXParser
{
	public static GeoCoordinate[] Parse(string file)
	{
		var doc = XDocument.Parse(file);
		var ns = doc.Root.GetDefaultNamespace();

		return doc.Root.Element(ns + "trk").Element(ns + "trkseg").Elements(ns + "trkpt")
			.Where(x => x.Attribute("lat") != null && x.Attribute("lon") != null)
			.Select(x => new GeoCoordinate
		{
			latitude = double.Parse(x.Attribute("lat").Value),
			longitude = double.Parse(x.Attribute("lon").Value),
			height = x.Element(ns + "ele") != null ? float.Parse(x.Element(ns + "ele").Value) : 0
		}).ToArray();
	}
}
