﻿using UnityEngine;
using System.Collections;
using VRStandardAssets.Utils;

[RequireComponent(typeof(VRInteractiveItem))]
public class GazeOrButtonSelector : MonoBehaviour
{
	[SerializeField] private bool autoActivate;

	[SerializeField] private SelectionMode mode;
	[SerializeField] private GameObject mainMenu;
	[SerializeField] private SelectionRadial selectionRadial;
	[SerializeField] private float selectionDuration = 1;

	private bool isActive;

	private VRInteractiveItem _interactiveItem;
	private VRInteractiveItem InteractiveItem
	{
		get { return _interactiveItem ?? (_interactiveItem = GetComponent<VRInteractiveItem>()); }
	}

	private void OnEnable()
	{
		InteractiveItem.OnOver += HandleOver;
		InteractiveItem.OnOut += HandleOut;
		selectionRadial.OnSelectionComplete += HandleSelectionComplete;
	}

	private void Start()
	{
		if (autoActivate)
		{
			HandleOver();
			HandleSelectionComplete();
		}
	}

	private void OnDisable()
	{
		InteractiveItem.OnOver -= HandleOver;
		InteractiveItem.OnOut -= HandleOut;
		selectionRadial.OnSelectionComplete -= HandleSelectionComplete;
	}

	private void HandleOver()
	{
		if (mode == SelectionMode.Gaze)
		{
			selectionRadial.SelectWithGaze = true;
		}
		else
		{
			selectionRadial.SelectWithGaze = false;
		}

		selectionRadial.SelectionDuration = selectionDuration;
		selectionRadial.Show();

		isActive = true;
	}

	private void HandleOut()
	{
		selectionRadial.Hide();

		isActive = false;
	}

	private void HandleSelectionComplete()
	{
		if (isActive)
		{
			mainMenu.SetActive(true);
			transform.root.gameObject.SetActive(false);
			selectionRadial.Hide();
		}
	}

	public enum SelectionMode
	{
		Button,
		Gaze
	}
}
