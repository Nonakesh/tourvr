﻿using UnityEngine;

public class MapData
{
	public Texture2D colorMap, heightMap;
	public double height;
}