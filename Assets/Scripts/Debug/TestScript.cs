﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(MapLoader))]
public class TestScript : MonoBehaviour
{
	void Start ()
	{
		GetComponent<MapLoader>().LoadTerrain(false, false);
	}
}
