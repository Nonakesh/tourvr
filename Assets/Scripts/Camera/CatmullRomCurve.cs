﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

//Modified from http://stackoverflow.com/questions/9489736/catmull-rom-curve-with-no-cusps-and-no-self-intersections
public static class CatmullRomCurves
{
	public enum CatmullRomType
	{
		Chordal,
		Uniform,
		Centripetal
	}

	/// <summary>
	/// This method will calculate the Catmull-Rom interpolation curve, returning 
	/// it as a list of Vector3 coordinate objects. This method in particular adds 
	/// the first and last control points which are not visible, but required for 
	/// calculating the spline.
	/// </summary>
	/// <param name="coordinates">The list of original straight line points to calculate 
	/// an interpolation from.</param>
	/// <param name="pointsPerSegment">The integer number of equally spaced points to return 
	/// along each curve.  The actual distance between each point will depend on the spacing 
	/// between the control points.</param>
	/// <param name="curveType">Chordal (stiff), Uniform(floppy), or Centripetal(medium)</param>
	/// <returns>The list of interpolated coordinates.</returns>
	public static List<Vector3> Interpolate(IList<Vector3> coordinates, int pointsPerSegment, CatmullRomType curveType)
	{
		if (pointsPerSegment < 2)
		{
			throw new ArgumentException("The pointsPerSegment parameter must be greater than 2, since 2 points is just the linear segment.");
		}

		// Cannot interpolate curves given only two points.  Two points
		// is best represented as a simple line segment.
		if (coordinates.Count < 3)
		{
			return new List<Vector3>(coordinates);
		}

		var vertices = ExtrapolateEndPoints(coordinates);

		// Dimension a result list of coordinates. 
		var result = new List<Vector3>();
		// When looping, remember that each cycle requires 4 points, starting
		// with i and ending with i+3.  So we don't loop through all the points.
		for (var i = 0; i < vertices.Count - 3; i++)
		{
			// Actually calculate the Catmull-Rom curve for one segment.
			var points = Interpolate(vertices, i, pointsPerSegment, curveType);
			// Since the middle points are added twice, once for each bordering
			// segment, we only add the 0 index result point for the first
			// segment.  Otherwise we will have duplicate points.
			if (result.Count > 0)
			{
				points.RemoveAt(0);
			}

			// Add the coordinates for the segment to the result list.
			result.AddRange(points);
		}
		return result;
	}

	public static List<Vector3> ExtrapolateEndPoints(IList<Vector3> coordinates)
	{
		var vertices = new List<Vector3>(coordinates);

		// Test whether the shape is open or closed by checking to see if
		// the first point intersects with the last point.  M and Z are ignored.
		var isClosed = Vector3.Distance(vertices[0], vertices[vertices.Count - 1]) < 0.1f;
		if (isClosed)
		{
			// Use the second and second from last points as control points.
			// get the second point.
			var p2 = vertices[1];
			// get the point before the last point
			var pn1 = vertices[vertices.Count - 2];

			// insert the second from the last point as the first point in the list
			// because when the shape is closed it keeps wrapping around to
			// the second point.
			vertices.Insert(0, pn1);
			// add the second point to the end.
			vertices.Add(p2);
		}
		else
		{
			// The shape is open, so use control points that simply extend
			// the first and last segments

			// Get the change in x and y between the first and second coordinates.
			var dx = vertices[1].x - vertices[0].x;
			var dy = vertices[1].y - vertices[0].y;

			// Then using the change, extrapolate backwards to find a control point.
			var x1 = vertices[0].x - dx;
			var y1 = vertices[0].y - dy;

			// Actaully create the start point from the extrapolated values.
			var start = new Vector3(x1, y1, vertices[0].z);

			// Repeat for the end control point.
			var n = vertices.Count - 1;
			dx = vertices[n].x - vertices[n - 1].x;
			dy = vertices[n].y - vertices[n - 1].y;
			var xn = vertices[n].x + dx;
			var yn = vertices[n].y + dy;
			var end = new Vector3(xn, yn);

			// insert the start control point at the start of the vertices list.
			vertices.Insert(0, start);

			// append the end control ponit to the end of the vertices list.
			vertices.Add(end);
		}

		return vertices;
	}

	/// <summary>
	/// Given a list of control points, this will create a list of pointsPerSegment
	/// points spaced uniformly along the resulting Catmull-Rom curve.
	/// </summary>
	/// <param name="points">The list of control points, leading and ending with a 
	/// coordinate that is only used for controling the spline and is not visualized.</param>
	/// <param name="index">The index of control point p0, where p0, p1, p2, and p3 are 
	/// used in order to create a curve between p1 and p2.</param>
	/// <param name="pointsPerSegment">The total number of uniformly spaced interpolated 
	/// points to calculate for each segment.The larger this number, the 
	/// smoother the resulting curve.</param>
	/// <param name="curveType">Clarifies whether the curve should use uniform, chordal 
	/// or centripetal curve types.Uniform can produce loops, chordal can 
	/// produce large distortions from the original lines, and centripetal is an 
	/// optimal balance without spaces.</param>
	/// <returns>the list of coordinates that define the CatmullRom curve 
	/// between the points defined by index+1 and index+2.</returns>
	public static List<Vector3> Interpolate(IList<Vector3> points, int index, int pointsPerSegment,
		CatmullRomType curveType)
	{
		var result = new List<Vector3>();
		var v = new Vector3[4];
		var time = new float[4];
		for (var i = 0; i < 4; i++)
		{
			v[i] = points[index + i];
			time[i] = i;
		}

		float tstart = 1;
		float tend = 2;
		if (curveType != CatmullRomType.Uniform)
		{
			float total = 0;
			for (var i = 1; i < 4; i++)
			{
				var dv = v[i] - v[i - 1];
				if (curveType == CatmullRomType.Centripetal)
				{
					total += Mathf.Pow(dv.magnitude, 0.5f);
				}
				else
				{
					total += dv.magnitude;
				}
				time[i] = total;
			}
			tstart = time[1];
			tend = time[2];
		}
		
		var segments = pointsPerSegment - 1;
		result.Add(points[index + 1]);
		for (var i = 1; i < segments; i++)
		{
			var xi = Interpolate(v.Select(vec => vec.x).ToArray(), time, tstart + i * (tend - tstart) / segments);
			var yi = Interpolate(v.Select(vec => vec.y).ToArray(), time, tstart + i * (tend - tstart) / segments);
			var zi = Interpolate(v.Select(vec => vec.z).ToArray(), time, tstart + i * (tend - tstart) / segments);
			result.Add(new Vector3(xi, yi, zi));
		}
		result.Add(points[index + 2]);
		return result;
	}

	public static Vector3 Interpolate(IList<Vector3> points, int index, CatmullRomType curveType, float t)
	{
		var result = new List<Vector3>();
		var v = new Vector3[4];
		var time = new float[4];
		for (var i = 0; i < 4; i++)
		{
			v[i] = points[index + i];
			time[i] = i;
		}

		float tstart = 1;
		float tend = 2;
		if (curveType != CatmullRomType.Uniform)
		{
			float total = 0;
			for (var i = 1; i < 4; i++)
			{
				var dv = v[i] - v[i - 1];
				float addition = 0;
				if (curveType == CatmullRomType.Centripetal)
				{
					addition = Mathf.Pow(dv.magnitude, 0.5f);
				}
				else
				{
					addition = dv.magnitude;
				}

				total += Mathf.Max(addition, 0.01f);
				time[i] = total;
			}
			tstart = time[1];
			tend = time[2];
		}

		var xi = Interpolate(v.Select(vec => vec.x).ToArray(), time, tstart + t * (tend - tstart));
		var yi = Interpolate(v.Select(vec => vec.y).ToArray(), time, tstart + t * (tend - tstart));
		var zi = Interpolate(v.Select(vec => vec.z).ToArray(), time, tstart + t * (tend - tstart));

		return new Vector3(xi, yi, zi);
	}

	/// <summary>
	/// Unlike the other implementation here, which uses the default "uniform" treatment of t, this computation 
	/// is used to calculate the same values but introduces the ability to "parameterize" the t values used in 
	/// the calculation. This is based on Figure 3 from http://www.cemyuksel.com/research/catmullrom_param/catmullrom.pdf
	/// </summary>
	/// <param name="p">An array of float values of length 4, where interpolation occurs from p1 to p2.</param>
	/// <param name="time">An array of time measures of length 4, corresponding to each p value.</param>
	/// <param name="t">the actual interpolation ratio from 0 to 1 representing the position between p1 and p2 to interpolate the value.</param>
	/// <returns>The interpolated value</returns>
	public static float Interpolate(float[] p, float[] time, float t)
	{
		var L01 = p[0] * (time[1] - t) / (time[1] - time[0]) + p[1] * (t - time[0]) / (time[1] - time[0]);
		var L12 = p[1] * (time[2] - t) / (time[2] - time[1]) + p[2] * (t - time[1]) / (time[2] - time[1]);
		var L23 = p[2] * (time[3] - t) / (time[3] - time[2]) + p[3] * (t - time[2]) / (time[3] - time[2]);
		var L012 = L01 * (time[2] - t) / (time[2] - time[0]) + L12 * (t - time[0]) / (time[2] - time[0]);
		var L123 = L12 * (time[3] - t) / (time[3] - time[1]) + L23 * (t - time[1]) / (time[3] - time[1]);
		var C12 = L012 * (time[2] - t) / (time[2] - time[1]) + L123 * (t - time[1]) / (time[2] - time[1]);
		return C12;
	}
}