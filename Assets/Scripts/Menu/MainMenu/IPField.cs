﻿using UnityEngine;
using System.Collections;
using System.Net;
using UnityEngine.UI;

public class IPField : MonoBehaviour
{
	public Text text;
	public GameObject error;
	public GameObject mainMenu;

	private string ip = "";

	public void Write(string sign)
	{
		ip += sign;
		text.text = ip;
	}

	public void Delete()
	{
		ip = ip.Substring(0, ip.Length-1);
		text.text = ip;
	}

	public void Clear()
	{
		ip = "";
		text.text = ip;
	}

	public void Accept()
	{
		IPAddress address;
		bool isValidIp = IPAddress.TryParse(ip, out address);

		if (isValidIp)
		{
			PlayerPrefs.SetString("ServerIP", ip);
			PlayerPrefs.Save();

			GoToMainMenu();
		}
		else
		{
			error.SetActive(true);
		}
	}

	private void GoToMainMenu()
	{
		mainMenu.SetActive(true);
		transform.root.gameObject.SetActive(false);
	}

	public void Start()
	{
		error.SetActive(false);

		if (!string.IsNullOrEmpty(PlayerPrefs.GetString("ServerIP")))
		{
			GoToMainMenu();
		}
	}
}
