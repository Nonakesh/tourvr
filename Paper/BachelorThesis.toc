\contentsline {section}{\numberline {1}Introduction}{1}{section.1}
\contentsline {subsection}{\numberline {1.1}Framework}{2}{subsection.1.1}
\contentsline {section}{\numberline {2}Related Work}{2}{section.2}
\contentsline {subsection}{\numberline {2.1}Viso Places}{2}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Google Maps}{2}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Google Earth VR}{2}{subsection.2.3}
\contentsline {section}{\numberline {3}User Interaction and Interfaces}{2}{section.3}
\contentsline {subsection}{\numberline {3.1}Motion Sickness}{2}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Interaction types}{2}{subsection.3.2}
\contentsline {section}{\numberline {4}Route Data}{3}{section.4}
\contentsline {subsection}{\numberline {4.1}Server architecture}{3}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Calculating a bounding box}{3}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}Conversion to engine units}{3}{subsection.4.3}
\contentsline {section}{\numberline {5}Terrain generation}{3}{section.5}
\contentsline {subsection}{\numberline {5.1}Levels of Detail}{3}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Generating Tiles}{4}{subsection.5.2}
\contentsline {subsection}{\numberline {5.3}Consequences of this method}{4}{subsection.5.3}
\contentsline {section}{\numberline {6}Loading Map Data}{4}{section.6}
\contentsline {subsection}{\numberline {6.1}Satellite images}{5}{subsection.6.1}
\contentsline {subsection}{\numberline {6.2}Heightmap}{5}{subsection.6.2}
\contentsline {subsubsection}{\numberline {6.2.1}Tile coordinate system}{5}{subsubsection.6.2.1}
\contentsline {subsubsection}{\numberline {6.2.2}Loading height maps}{5}{subsubsection.6.2.2}
\contentsline {subsubsection}{\numberline {6.2.3}Accessing height information}{6}{subsubsection.6.2.3}
\contentsline {subsubsection}{\numberline {6.2.4}Decoding height map data}{6}{subsubsection.6.2.4}
\contentsline {section}{\numberline {7}Visual improvements}{6}{section.7}
\contentsline {section}{\numberline {8}Movement}{6}{section.8}
\contentsline {subsection}{\numberline {8.1}Teleportation Based Movement}{6}{subsection.8.1}
\contentsline {subsection}{\numberline {8.2}Guided Movement}{7}{subsection.8.2}
\contentsline {subsection}{\numberline {8.3}Aerial Movement}{7}{subsection.8.3}
\contentsline {section}{\numberline {9}Future improvements}{7}{section.9}
\contentsline {subsection}{\numberline {9.1}Terrain rendering for flights}{7}{subsection.9.1}
\contentsline {subsection}{\numberline {9.2}Server architecture}{7}{subsection.9.2}
\contentsline {subsection}{\numberline {9.3}Environmental cubemaps}{7}{subsection.9.3}
\contentsline {section}{\numberline {10}Conclusion}{7}{section.10}
