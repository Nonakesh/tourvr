﻿using System.Linq;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using UnityEngine;

public static class KMLParser
{
	public static GeoCoordinate[] Parse(string file)
	{
		var doc = XDocument.Parse(file);
		var ns = doc.Root.GetDefaultNamespace();

		string allCoords = doc.Descendants(ns + "coordinates").First().Value;
		var coords = Regex.Split(allCoords, "\r\n|\r|\n")
			.Select(x => x.Trim())
			.Where(x => !string.IsNullOrEmpty(x));
		return coords.Select(x => x.Split(','))
			.Select(x => new GeoCoordinate
		{
			longitude = double.Parse(x[0]),
			latitude = double.Parse(x[1]),
			height = float.Parse(x[2])
		}).ToArray();
	}
}
