﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ErrorDisabler : MonoBehaviour
{
	public float disableTime = 5;
	private float startTime;

	private void OnEnable ()
	{
		startTime = Time.time;
	}
	
	void Update ()
	{
		if (startTime + disableTime < Time.time)
		{
			gameObject.SetActive(false);
		}
	}
}
