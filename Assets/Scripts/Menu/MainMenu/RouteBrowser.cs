﻿using System.Collections;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class RouteBrowser : MonoBehaviour
{
	public Text[] buttonText;

	public GameObject currentRoot;
	public GameObject mapLoaderParent;
	public LoadingScreen loadingScreen;
	public MapLoader mapLoader;
	public CameraController cameraController;

	public TerrainGenerator generator;
	public MapExit exit;

	private string[] routePaths;
	private int page;

	public int PageCount
	{
		get { return Mathf.CeilToInt((float)routePaths.Length / buttonText.Length); }
	}

	private void OnEnable()
	{
		Refresh();
	}

	public void Refresh()
	{
		RefreshData();
	}

	private void RefreshData()
	{
		string path = Application.persistentDataPath + "/" + "Routes";

		if (!Directory.Exists(path))
		{
			Directory.CreateDirectory(path);
		}

		routePaths = Directory.GetFiles(path);
		page = 0;

		UpdateText();
	}

	private void UpdateText()
	{
		int pageStart = buttonText.Length * page;
		for (int i = 0; i < buttonText.Length; i++)
		{
			if (i + pageStart < routePaths.Length)
			{
				buttonText[i].text = routePaths[i + pageStart].Split('/','\\').Last().Split('.')[0].Replace('_', ' ').Replace('-', ' ');
			}
			else
			{
				buttonText[i].text = "No Route";
			}
		}
	}

	public void NextPage()
	{
		if (PageCount == 0)
		{
			return;
		}

		page = Mathd.Mod(page + 1, PageCount);
		UpdateText();
	}

	public void PrevPage()
	{
		if (PageCount == 0)
		{
			return;
		}

		page = Mathd.Mod(page - 1, PageCount);
		UpdateText();
	}

	public void LoadRoute(int buttonId)
	{
		int pageStart = buttonText.Length * page;

		if (routePaths == null || pageStart + buttonId >= routePaths.Length)
		{
			return;
		}

		string routePath = routePaths[pageStart + buttonId];

		bool flightMode = routePath.ToLower().EndsWith(".igc");
		cameraController.FlightMode = flightMode;

		//Set current map loader
		exit.mapLoader = mapLoader;
		generator.SetMapLoader(mapLoader);
		cameraController.mapLoader = mapLoader;

		mapLoaderParent.SetActive(true);

		//Start loading
		loadingScreen.gameObject.SetActive(true);
		loadingScreen.StartLoading();

		currentRoot.SetActive(false);

		mapLoader.offlineData = File.ReadAllText(routePath);
		mapLoader.offlineRouteName = routePath;

		mapLoader.offlineMode = true;
		mapLoader.LoadTerrain(flightMode, false);
	}
}
