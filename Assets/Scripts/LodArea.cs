using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class LodArea
{
	public GeoBoundingBox bounds;
	public Texture2D colorMap;
	public TextureGrid heightMaps;
	public int zoomLevel;
	public GeoTileCoordinate tileStart, tileEnd;
	public int worldTileCount;
	public int heightmapStartX;
	public int heightmapStartY;
	public MapLoader.LodLevelSettings lodLevel;
}

[Serializable]
public class TextureGrid : IEnumerable<Texture2D>
{
	[SerializeField]
	private Texture2D[] textures;

	[SerializeField]
	private int width;
	[SerializeField]
	private int height;

	public TextureGrid(int width, int height)
	{
		this.width = width;
		this.height = height;
		textures = new Texture2D[width * height];
	}

	public Texture2D this[int i]
	{
		get { return textures[i]; }

		set { textures[i] = value; }
	}

	public Texture2D this[int x, int y]
	{
		get { return textures[x + y * width]; }

		set { textures[x + y * width] = value; }
	}

	public int Length
	{
		get { return textures.Length; }
	}

	public int Width
	{
		get { return width; }
	}

	public int Height
	{
		get { return Height; }
	}

	IEnumerator IEnumerable.GetEnumerator()
	{
		return GetEnumerator();
	}

	public IEnumerator<Texture2D> GetEnumerator()
	{
		return ((IEnumerable<Texture2D>)textures).GetEnumerator();
	}
}