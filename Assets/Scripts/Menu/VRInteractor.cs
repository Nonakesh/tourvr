﻿using UnityEngine;
using VRStandardAssets.Utils;

public abstract class VRInteractor : MonoBehaviour
{
	[SerializeField] private SelectionRadial selectionRadial;
	[SerializeField] private bool isRepeatableButton = false;


	private bool isActive;

	private VRInteractiveItem _interactiveItem;
	protected VRInteractiveItem InteractiveItem
	{
		get { return _interactiveItem ?? (_interactiveItem = GetComponent<VRInteractiveItem>()); }
	}

	private void OnEnable()
	{
		InteractiveItem.OnOver += HandleOver;
		InteractiveItem.OnOut += HandleOut;
		selectionRadial.OnSelectionComplete += HandleSelectionComplete;
	}

	private void OnDisable()
	{
		InteractiveItem.OnOver -= HandleOver;
		InteractiveItem.OnOut -= HandleOut;
		selectionRadial.OnSelectionComplete -= HandleSelectionComplete;
	}

	private void HandleOver()
	{
		selectionRadial.Show();

		isActive = true;
	}

	private void HandleOut()
	{
		selectionRadial.Hide();

		isActive = false;
	}

	private void HandleSelectionComplete()
	{
		if (isActive)
		{
			isActive = false;
			selectionRadial.Hide();
			OnSelectionCompleted();

			if (isRepeatableButton)
			{
				selectionRadial.Show();
				isActive = true;
			}
		}
	}

	protected abstract void OnSelectionCompleted();
}