﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;

public static class IGCParser
{
	public static GeoCoordinate[] Parse(string file)
	{
		string[] lines = Regex.Split(file, "\r\n|\r|\n");

		var coords = new List<GeoCoordinate>();
		foreach (var line in lines)
		{
			//Ignore all lines that don't include a record
			if (!line.StartsWith("B"))
			{
				continue;
			}

			var coord = new GeoCoordinate
			{
				time = int.Parse(line.Substring(1, 2)) * 3600
						+ int.Parse(line.Substring(3, 2)) * 60
						+ int.Parse(line.Substring(5, 2)),
				latitude = ToDecimalDegree(int.Parse(line.Substring(7, 2)), 
										   int.Parse(line.Substring(9, 2)),
										   int.Parse(line.Substring(11, 3)))
										   * (line[14] == 'N' ? 1 : -1),
				longitude = ToDecimalDegree(int.Parse(line.Substring(15, 3)),
										    int.Parse(line.Substring(18, 2)),
										    int.Parse(line.Substring(20, 3)))
										    * (line[23] == 'E' ? 1 : -1),
			};

			if (line[24] == 'A')
			{
				//Get height from pressure sensor
				int altitude = int.Parse(line.Substring(25, 5));

				//If pressure sensor returns nothing, use gps altitude
				if (altitude == 0)
				{
					altitude = int.Parse(line.Substring(30, 5));
				}

				coord.height = altitude;
			}

			coords.Add(coord);
		}

		return coords.ToArray();
	}

	private static double ToDecimalDegree(int degree, int minutes, int minuteDecimal)
	{
		return degree + (minutes + minuteDecimal / 1000.0) / 60.0;
	}
}
