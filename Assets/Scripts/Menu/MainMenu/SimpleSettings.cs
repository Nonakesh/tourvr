﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class SimpleSettings : MonoBehaviour
{
	public Text TextField;

	public const string TerrainSettings = "TerrainSetting";
	public SettingSet[] Settings;

	private int currentSetting;

	public void Start()
	{
		int setting = PlayerPrefs.GetInt(TerrainSettings, 0);
		ApplySetting(setting);
	}

	public void NextSetting()
	{
		ApplySetting(currentSetting + 1);
	}

	public void PrevSetting()
	{
		ApplySetting(currentSetting - 1);
	}

	public void ApplySetting(int index)
	{
		index = Mathf.Clamp(index, 0, Settings.Length-1);

		PlayerPrefs.SetInt(TerrainSettings, index);

		currentSetting = index;
		var setting = Settings[index];

		TextField.text = setting.SettingLevel;

		foreach (var s in setting.Settings)
		{
			PlayerPrefs.SetInt(s.SettingName, s.IntValue);
			//switch (s.Type)
			//{
			//	case SettingType.Int:
			//		PlayerPrefs.SetInt(s.SettingName, s.IntValue);
			//		break;
			//	case SettingType.Float:
			//		PlayerPrefs.SetFloat(s.SettingName, s.FloatValues);
			//		break;
			//	case SettingType.String:
			//		PlayerPrefs.SetString(s.SettingName, s.StringValue);
			//		break;
			//	default:
			//		throw new ArgumentOutOfRangeException();
			//}
		}
	}
}

[Serializable]
public class SettingSet
{
	public string SettingLevel;
	public SimpleSetting[] Settings;
}

[Serializable]
public class SimpleSetting
{
	public string SettingName;
	//public SettingType Type;
	//public string StringValue;
	public int IntValue;
	//public float FloatValues;
}

public enum SettingType
{
	Int,
	Float,
	String
}