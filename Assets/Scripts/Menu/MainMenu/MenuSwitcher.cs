﻿using UnityEngine;
using System.Collections;
using VRStandardAssets.Utils;

[RequireComponent(typeof(VRInteractiveItem))]
public class MenuSwitcher : VRInteractor
{
	public GameObject currentMenuItem;
	public GameObject otherMenuItem;

	protected override void OnSelectionCompleted()
	{
		otherMenuItem.SetActive(true);
		currentMenuItem.SetActive(false);
	}
}
