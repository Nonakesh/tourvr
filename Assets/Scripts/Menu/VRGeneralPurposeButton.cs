﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using VRStandardAssets.Utils;

[RequireComponent(typeof(VRInteractiveItem))]
public class VRGeneralPurposeButton : VRInteractor
{
	[SerializeField] private UnityEvent buttonEvent;

	protected override void OnSelectionCompleted()
	{
		buttonEvent.Invoke();
	}
}
